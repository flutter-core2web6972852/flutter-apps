import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ViewEmpData extends StatefulWidget {
  const ViewEmpData({super.key});

  @override
  State<ViewEmpData> createState() => _ViewempdataState();
}

class _ViewempdataState extends State<ViewEmpData> {
  Map empData = {};
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Api Binding"),
        centerTitle: true,
      ),
      body: ListView.builder(
          itemCount: 1,
          itemBuilder: (context, index) {
            return Row(
              children: [
                Text(empData['employee_name']),
                const SizedBox(
                  width: 20,
                ),
                Text("${empData['employee_salary']}")
              ],
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () => getEmpData(),
        child: const Icon(Icons.add),
      ),
    );
  }

  void getEmpData() async {
    Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/employee/10");
    http.Response response = await http.get(url);
    var responseData = json.decode(response.body);
    log("hii");
    log(response.body); 
    setState(() {
      empData = responseData['data'];
    });
    log("$empData");
  }
}
