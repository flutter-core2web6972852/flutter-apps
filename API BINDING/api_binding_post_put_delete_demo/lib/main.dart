import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Api bidnig demo"),
          centerTitle: true,
          backgroundColor: Colors.grey,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => getData(),
          child: const Icon(Icons.add),
        ),
      ),
    );
  }

  void getData() async {
    Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/delete/2");
    http.Response response = await http.delete(url);
    log(response.body);
  }
}
