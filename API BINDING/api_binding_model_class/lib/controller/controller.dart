import 'dart:convert';
import 'package:http/http.dart' as http;

Future getEmpData() async {
  Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/employees");
  http.Response response = await http.get(url);
  var responseData = json.decode(response.body);
  return responseData;
}
