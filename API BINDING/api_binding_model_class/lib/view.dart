import 'package:api_binding_model_class/controller/controller.dart';
import 'package:flutter/material.dart';
import 'model/model.dart';

class ViewEmpData extends StatefulWidget {
  const ViewEmpData({super.key});

  @override
  State<ViewEmpData> createState() => _ViewEmpDataState();
}

class _ViewEmpDataState extends State<ViewEmpData> {
  List<Data> data = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Api binding get data with model class"),
        backgroundColor: Colors.grey,
      ),
      body: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return Row(
              children: [
                Text(data[index].empName!),
                const SizedBox(
                  width: 10,
                ),
                Text("${data[index].empSal}"),
              ],
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var responseData = await getEmpData();
          Employee_model obj = Employee_model(responseData);

          setState(() {
            data = obj.data!;
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
