import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/view/second.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> imaages = [
    "assets/home.png",
    "assets/home.png",
    "assets/home.png",
    "assets/home.png"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/home.png"),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 440, left: 44, right: 45),
              child: Text(
                "Dancing between The shadows Of rhythm ",
                style: GoogleFonts.inter(
                    fontWeight: FontWeight.w600,
                    fontSize: 36,
                    color: const Color.fromRGBO(255, 255, 255, 1)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 28),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const SecondScreen()));
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 47,
                  width: 261,
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(255, 46, 0, 1),
                      borderRadius: BorderRadius.circular(19)),
                  child: Text(
                    "Get started",
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: const Color.fromRGBO(19, 19, 19, 1)),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16,
              ),
              child: GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  height: 47,
                  width: 261,
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(19, 19, 19, 1),
                      border: Border.all(
                          color: const Color.fromRGBO(255, 61, 0, 1)),
                      borderRadius: BorderRadius.circular(19)),
                  child: Text(
                    "Continue with Email",
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: const Color.fromRGBO(255, 46, 0, 1)),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 21,
              ),
              child: GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  height: 35,
                  width: 227,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(19)),
                  child: Text(
                    "by continuing you agree to terms of services and  Privacy policy",
                    style: GoogleFonts.inter(
                      fontWeight: FontWeight.w600,
                      fontSize: 13,
                      color: const Color.fromRGBO(203, 200, 200, 1),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
