import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:carousel_slider/carousel_slider.dart';

class SecondScreen extends StatefulWidget {
  const SecondScreen({super.key});

  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  final List imageList = [
    {"id": 1, "image_path": "assets/alone.png"},
    {"id": 2, "image_path": "assets/alone.png"},
    {"id": 3, "image_path": "assets/alone.png"},
    {"id": 3, "image_path": "assets/alone.png"}
  ];
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 255, 255, 0.04),
      body: Column(
        children: [
          Stack(
            children: [
              CarouselSlider(
                items: imageList
                    .map((item) => Image.asset(
                          height: 367,
                          item['image_path'],
                          fit: BoxFit.cover,
                          width: double.infinity,
                        ))
                    .toList(),
                carouselController: _controller,
                options: CarouselOptions(
                  scrollPhysics: const BouncingScrollPhysics(),
                  autoPlay: true,
                  aspectRatio: 1,
                  viewportFraction: 1,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 225, left: 20),
                child: Column(
                  children: [
                    Text(
                      "A.L.O.N.E",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 36,
                          color: Colors.white),
                    )
                  ],
                ),
              )
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imageList.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => _controller.animateToPage(entry.key),
                child: Container(
                  width: _current == entry.key ? 21 : 7,
                  height: 7.0,
                  margin: const EdgeInsets.symmetric(horizontal: 3.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: _current == entry.key
                        ? const Color.fromRGBO(255, 61, 0, 1)
                        : const Color.fromRGBO(159, 159, 159, 1),
                  ),
                ),
              );
            }).toList(),
          )
        ],
      ),
    );
  }
}
