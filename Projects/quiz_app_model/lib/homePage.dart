import 'package:flutter/material.dart';
import 'package:quiz_app_model/quizapp.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "QuizApp",
          style: TextStyle(
              fontSize: 30, fontWeight: FontWeight.w800, color: Colors.red),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Container(
                height: 100,
                width: 300,
                child: const Text(
                  "Hello Rohit,",
                  style: TextStyle(
                    fontSize: 30,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
              height: 100,
              width: 100,
              child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0KqU7xtLm15TbRb_2FbXvF1I_QTf3-Wxh70aLzWwQPAbIABV1wlWFWZW7NweoVBwjEgQ&usqp=CAU")
           ),
           const Text("Welocome Back!!",
           style: TextStyle(fontWeight: FontWeight.bold),),
          const SizedBox(
           height: 30,
          ),
           ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>  const QuizApp(),
                                ));
                          },
                          style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Colors.blue)),
                          child: const Text(
                            "Start Quiz Now!",
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                fontWeight: FontWeight.normal),
                          )),
        ],
      ),
    );
  }
}
