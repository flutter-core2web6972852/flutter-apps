import 'package:flutter/material.dart';
import 'package:quiz_app_model/homePage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});
  @override
  State createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State {
  final GlobalKey<FormFieldState> _usernamekey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _passwordkey = GlobalKey<FormFieldState>();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  Map logindetails = {
    "rohitmore1902@gmail.com": "Rohit@1926",
    "shivanjali": "Shiv@123"
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Login Page",
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        controller: PageController(),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
          child: Container(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
            width: double.infinity,
            decoration: const BoxDecoration(
                color: Color.fromARGB(255, 232, 226, 226),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(100),
                  topRight: Radius.circular(100),
                )),
            child: Column(
              children: [
                const Text(
                  "Welcome Back",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25),
                ),
                const SizedBox(
                  height: 20,
                ),
                Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0KqU7xtLm15TbRb_2FbXvF1I_QTf3-Wxh70aLzWwQPAbIABV1wlWFWZW7NweoVBwjEgQ&usqp=CAU"),
                const SizedBox(height: 20),
                TextFormField(
                  key: _usernamekey,
                  controller: _username,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "enter username";
                    } else {
                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      labelText: "Enter username",
                      prefixIcon: Icon(Icons.email_outlined)),
                  keyboardType: TextInputType.emailAddress,
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  key: _passwordkey,
                  controller: _password,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "enter password";
                    } else {
                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      labelText: "Enter password",
                      prefixIcon: Icon(Icons.lock_outline_rounded)),
                  obscureText: true,
                  obscuringCharacter: "*",
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                    height: 60,
                    width: double.infinity,
                    child: ElevatedButton(
                        onPressed: () {
                          bool usernamevalidated =
                              _usernamekey.currentState!.validate();
                          bool passwordvalidated =
                              _passwordkey.currentState!.validate();

                          if (usernamevalidated && passwordvalidated) {
                            if (logindetails.containsKey(_username.text) &&
                                logindetails.containsValue(_password.text)) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text('Login SuccessFully')));
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const HomePage()));
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content:
                                          Text('Please Enter Valid Details!')));
                            }
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(content: Text('Login Failed')));
                          }
                        },
                        child: const Text("Login"))),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "or continue with",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 100),
                Container(
                  height: 70,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(100),
                        topRight: Radius.circular(100),
                      )),
                  child: TextButton(
                      onPressed: () {
                        showModalBottomSheet(
                            context: context,
                            builder: ((context) => Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 10, 20, 0),
                                  width: double.infinity,
                                  decoration: const BoxDecoration(
                                      color: Color.fromARGB(255, 232, 226, 226),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(30),
                                        topRight: Radius.circular(30),
                                      )),
                                  child: Column(children: [
                                    const Text(
                                      "Welcome!",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 25),
                                    ),
                                    const SizedBox(height: 20),
                                    Image.asset("assets/demo.png"),
                                    const SizedBox(height: 20),
                                  ]),
                                ))));
                      },
                      child: const Text("Dont have an account? Sign up")),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
