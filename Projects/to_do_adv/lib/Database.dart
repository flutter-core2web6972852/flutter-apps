
// ignore_for_file: non_constant_identifier_names, file_names

import 'package:sqflite/sqflite.dart';

dynamic database;
List<ToDoList1> maplist = List.empty(growable: true);
List<LoginModel> loginDetails = List.empty(growable: true);

class LoginModel {
  final int? id;
  final String userName;
  final String password;
  LoginModel({this.id, required this.userName, required this.password});

  Map<String, dynamic> tomap() {
    return {
      'username': userName,
      'password': password,
    };
  }

  @override
  String toString() {
    return '''id:$id,
    userName:$userName,
    password:$password,
    ''';
  }
}

class ToDoList1 {
  int? id;
  String title;
  String description;
  String date;

  ToDoList1(
      {this.id,
      required this.title,
      required this.description,
      required this.date});

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
    };
  }

  @override
  String toString() {
    return '{id:$id,title:$title,description:$description,experience:$date}';
  }
}

Future<void> addUser(LoginModel obj) async {
  final localdb = await database;

  await localdb.insert(
    "Login",
    obj.tomap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<LoginModel>> getData() async {
  final Localdb = await database;

  List<Map<String, dynamic>> details = await Localdb.query("Login");

  return List.generate(
    details.length,
    (i) => LoginModel(
        id: details[i]['id'],
        userName: details[i]['userName'],
        password: details[i]['password']),
  );
}

Future insertNewTask(ToDoList1 obj) async {
  final localdb = await database;

  await localdb.insert(
    "task",
    obj.toMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<ToDoList1>> getAllTask() async {
  final localdb = await database;

  List<Map<String, dynamic>> taskList = await localdb.query("task");
  return List.generate(
      taskList.length,
      (i) => ToDoList1(
          id: taskList[i]['id'],
          title: taskList[i]['title'],
          description: taskList[i]['description'],
          date: taskList[i]['date']));
}

Future<void> updatetask(ToDoList1 obj) async {
  final db = await database;

  await db.update(
    'task',
    obj.toMap(),
    where: 'id = ?',
    whereArgs: [obj.id],
  );
}

Future<void> deletetask(int id) async {
  final db = await database;

  await db.delete(
    'task',
    where: 'id = ?',
    whereArgs: [id],
  );
}