import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';
import 'package:to_do_adv/Login.dart';
import 'Database.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = openDatabase(
    path.join(await getDatabasesPath(),"ToDo4.db"),
    version: 1,
    onCreate: (db,version) async {
      db.execute('''
       CREATE TABLE Login(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        userName TEXT,
        password TEXT
      )
''');
      db.execute('''CREATE TABLE task
          (id INTEGER PRIMARY KEY AUTOINCREMENT,
          title TEXT,
          description Text,
           date INTEGER) ''');
    },
  );
  loginDetails = await getData();
  maplist = await getAllTask();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const LoginPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}



