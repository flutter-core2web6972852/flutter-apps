import 'package:flutter/material.dart';

class User extends InheritedWidget {
  final String username;

  const User({
    super.key,
    required this.username,
    required super.child,
  });

  static User of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType()!;
  }

  @override
  bool updateShouldNotify(User oldWidget) {
    return username != oldWidget.username;
  }
}
