import 'package:flutter/material.dart';
import 'inheritedwidget.dart';
import 'profile.dart';

class HomeScreen extends StatelessWidget {


  const HomeScreen({super.key,});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
            const Text(
              'Welcome,',
            ),
            const UsernameDisplay(),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ProfileScreen(),
                  ),
                );
              },
              child: const Text('View Profile'),
            ),
          ],
        ),
      ),
    );
  }
}
class UsernameDisplay extends StatelessWidget {
  const UsernameDisplay({super.key});

  @override
  Widget build(BuildContext context) {
    final username = User.of(context).username;
    return Text(
      username,
      style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
    );
  }
}
