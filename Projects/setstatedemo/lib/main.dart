import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {

    
    return const MaterialApp(
      home: Variable(),
    );
  }
}

// ignore: camel_case_types
class Variable extends StatefulWidget {
  final int data = 50;
  const Variable({super.key});

  @override
  State<Variable> createState() => _variableState();
}

// ignore: camel_case_types
class _variableState extends State<Variable> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("${widget.data}"),
        ],
      ),
    );
  }
}
