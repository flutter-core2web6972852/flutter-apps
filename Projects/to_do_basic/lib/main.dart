import 'package:flutter/material.dart';
import 'package:to_do/todo.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});
  @override
  State createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State {
  final GlobalKey<FormFieldState> _usernamekey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _passwordkey = GlobalKey<FormFieldState>();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final GlobalKey<FormFieldState> _emailKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _passKey = GlobalKey<FormFieldState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  Map logindetails = {
    "rohitmore1902@gmail.com": "Rohit@1926",
    "shivanjali": "Shiv@123"
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "To do list",
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 209, 172, 216),
        shape: const BeveledRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10))),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
          child: Container(
              height: 800,
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
              width: double.infinity,
              decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 232, 226, 226),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(100),
                    topRight: Radius.circular(100),
                  )),
              child: Column(children: [
                const Text(
                  "Welcome Back",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25),
                ),
                const SizedBox(
                  height: 20,
                ),
                Image.asset("assets/demo.png"),
                const SizedBox(height: 20),
                TextFormField(
                  key: _usernamekey,
                  controller: _username,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "enter username";
                    } else {
                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      labelText: "Enter username",
                      prefixIcon: Icon(Icons.email_outlined)),
                  keyboardType: TextInputType.emailAddress,
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  key: _passwordkey,
                  controller: _password,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "enter password";
                    } else {
                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      labelText: "Enter password",
                      prefixIcon: Icon(Icons.lock_outline_rounded)),
                  obscureText: true,
                  obscuringCharacter: "*",
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                    height: 60,
                    width: double.infinity,
                    child: ElevatedButton(
                        onPressed: () {
                          bool usernamevalidated =
                              _usernamekey.currentState!.validate();
                          bool passwordvalidated =
                              _passwordkey.currentState!.validate();

                          if (usernamevalidated && passwordvalidated) {
                            if (logindetails.containsKey(_username.text) &&
                                logindetails.containsValue(_password.text)) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text('Login SuccessFully')));
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return const TodoList();
                              }));
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content:
                                          Text('Please Enter Valid Details!')));
                            }
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text(
                                        'Login Failed! Please enter details')));
                          }
                        },
                        child: const Text("Login"))),
                const SizedBox(
                  height: 20,
                ),
                TextButton(
                    onPressed: () {
                      showModalBottomSheet(
                          backgroundColor:
                              const Color.fromARGB(255, 232, 226, 226),
                          isScrollControlled: true,
                          context: context,
                          builder: (context) {
                            return Padding(
                              padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom,
                                left: 10,
                                right: 10,
                                top: 10,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    "Create Account",
                                    style: GoogleFonts.quicksand(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 22,
                                        color:
                                            const Color.fromRGBO(0, 0, 0, 1)),
                                  ),
                                  Image.asset("assets/demo.png"),
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Email",
                                          style: GoogleFonts.quicksand(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 13,
                                          ),
                                        ),
                                        TextFormField(
                                          key: _emailKey,
                                          controller: _emailController,
                                          validator: (value) {
                                            if (value == null ||
                                                value.isEmpty) {
                                              return "Please Enter Email";
                                            } else {
                                              return null;
                                            }
                                          },
                                          decoration: const InputDecoration(
                                              border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                          )),
                                        ),
                                        Text(
                                          "Password",
                                          style: GoogleFonts.quicksand(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 13,
                                          ),
                                        ),
                                        TextFormField(
                                          obscureText: true,
                                          obscuringCharacter: "*",
                                          key: _passKey,
                                          controller: _passwordController,
                                          validator: (value) {
                                            if (value == null ||
                                                value.isEmpty) {
                                              return "Please Enter password";
                                            } else {
                                              return null;
                                            }
                                          },
                                          decoration: const InputDecoration(
                                              border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                          )),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        SizedBox(
                                          width: double.infinity,
                                          child: ElevatedButton(
                                            onPressed: () {
                                              bool emailFlag = _emailKey
                                                  .currentState!
                                                  .validate();
                                              bool passFlag = _passKey
                                                  .currentState!
                                                  .validate();

                                              if (emailFlag && passFlag) {
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(const SnackBar(
                                                        content: Text(
                                                            "Account Created Successfully")));
                                                Navigator.of(context).pop();
                                              } else {
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(const SnackBar(
                                                        content: Text(
                                                            "Account Creation Failed")));
                                              }
                                            },
                                            style: const ButtonStyle(
                                                backgroundColor:
                                                    MaterialStatePropertyAll(
                                                        Color.fromRGBO(
                                                            89, 57, 241, 1))),
                                            child: const Text(
                                              "Submit",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 20,
                                              ),
                                            ),
                                          ),
                                        )
                                      ])
                                ],
                              ),
                            );
                          });
                    },
                    child: const Text(
                      "Don't have an account? Sign up",
                      style: TextStyle(color: Colors.blue, fontSize: 15),
                    ))
              ])),
        ),
      ),
    );
  }
}
