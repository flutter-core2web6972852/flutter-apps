import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class TodoList extends StatefulWidget {
  const TodoList({super.key});

  @override
  State<TodoList> createState() => _TodoListState();
}

class ToDoModel {
  String title;
  String description;
  String date;

  ToDoModel({
    required this.title,
    required this.description,
    required this.date,
  });
}

class _TodoListState extends State<TodoList> {
  bool isEdit = false;

  TextEditingController titlecontroller = TextEditingController();
  TextEditingController descriptioncontroller = TextEditingController();
  TextEditingController dateController = TextEditingController();

  List cardColor = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
  ];
  List task = List.empty(growable: true);

  void edittask(bool isEdit, ToDoModel toDoModel) {
    titlecontroller.text = toDoModel.title;
    descriptioncontroller.text = toDoModel.description;
    dateController.text = toDoModel.date;

    showBottomSheet1(isEdit, toDoModel);
  }

  void submit(bool isEdit, [ToDoModel? toDoModel]) {
    if (titlecontroller.text.trim().isNotEmpty &&
        descriptioncontroller.text.trim().isNotEmpty &&
        dateController.text.trim().isNotEmpty) {
      if (!isEdit) {
        setState(() {
          task.add(ToDoModel(
              title: titlecontroller.text,
              description: descriptioncontroller.text,
              date: dateController.text));
        });
      } else {
        setState(() {
          toDoModel!.title = titlecontroller.text;
          toDoModel.description = descriptioncontroller.text;
          toDoModel.date = dateController.text;
        });
      }
    } else {
      return;
    }
    titlecontroller.clear();
    descriptioncontroller.clear();
    dateController.clear();
    Navigator.of(context).pop();
  }

  void showBottomSheet1(bool isEdit, [ToDoModel? toDoModel]) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
              padding: EdgeInsets.only(
                  left: 15,
                  right: 15,
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Create Task",
                    style: GoogleFonts.quicksand(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                    ),
                  ),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Title",
                          style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              color: const Color.fromRGBO(0, 139, 148, 1)),
                        ),
                        TextField(
                            controller: titlecontroller,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(style: BorderStyle.solid),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                            )),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Description",
                          style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              color: const Color.fromRGBO(0, 139, 148, 1)),
                        ),
                        TextField(
                            controller: descriptioncontroller,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                            )),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Date",
                          style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              color: const Color.fromRGBO(0, 139, 148, 1)),
                        ),
                        TextField(
                            controller: dateController,
                            onTap: () async {
                              DateTime? pickedDate = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(2024),
                                  lastDate: DateTime(2025));

                              String formatedDate =
                                  DateFormat.yMMMd().format(pickedDate!);
                              setState(() {
                                dateController.text = formatedDate;
                              });
                            },
                            decoration: const InputDecoration(
                              suffixIcon: Icon(Icons.calendar_month_outlined),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                            )),
                        const SizedBox(
                          height: 10,
                        ),
                      ]),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        if (!isEdit) {
                          submit(false);
                        } else {
                          submit(true, toDoModel);
                        }
                      },
                      style: const ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(
                              Color.fromRGBO(0, 132, 177, 1))),
                      child: Text(
                        "Submit",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            color: const Color.fromRGBO(255, 255, 255, 1)),
                      ),
                    ),
                  ),
                ],
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "To do list",
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 209, 172, 216),
        shape: const BeveledRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10))),
      ),
      body: (task.isNotEmpty)
          ? Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              child: ListView.builder(
                itemCount: task.length,
                itemBuilder: (context, index) => Container(
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.only(bottom: 20),
                  decoration: BoxDecoration(
                    color: cardColor[index % cardColor.length],
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 80,
                            height: 80,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              // borderRadius: BorderRadius.circular(100),
                              border: Border.all(
                                color: Colors.white,
                                width: 5,
                              ),
                            ),
                            child: const Icon(Icons.image),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  task[index].title,
                                  style: GoogleFonts.quicksand(
                                    textStyle: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  task[index].description,
                                  style: GoogleFonts.quicksand(
                                    textStyle: const TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          const SizedBox(
                            width: 10,
                          ),
                          Text(task[index].date),
                          const Spacer(),
                          Row(
                            children: [
                              IconButton(
                                onPressed: () {
                                  edittask(true, task[index]);
                                },
                                icon: const Icon(
                                  Icons.edit_outlined,
                                  color: Color.fromRGBO(
                                    2,
                                    167,
                                    177,
                                    1,
                                  ),
                                ),
                              ),
                              IconButton(
                                onPressed: () {
                                  task.removeAt(index);
                                  setState(() {});
                                },
                                icon: const Icon(
                                  Icons.delete_outline_rounded,
                                  color: Color.fromRGBO(
                                    2,
                                    167,
                                    177,
                                    1,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            )
          : const Padding(
              padding: EdgeInsets.all(10),
              child: Center(child: Text("No Task ")),
            ),
      floatingActionButton: FloatingActionButton(
        tooltip: "Add Task",
        onPressed: () {
          showBottomSheet1(false);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
