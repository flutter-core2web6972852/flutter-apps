import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'SecondPage.dart';
//import 'package:flutter_svg/flutter_svg.dart';

class Getstart_page extends StatefulWidget {
  const Getstart_page({super.key});

  @override
  State<Getstart_page> createState() => _Getstart_pageState();
}

class _Getstart_pageState extends State<Getstart_page> {
  bool getstart = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              height: 42,
              width: 570,
              color: const Color.fromRGBO(123, 123, 123, 1),
            ),
            const SizedBox(
              height: 15,
            ),
            // SvgPicture.asset("images/1.svg"),
            Image.asset("images/1same.jpg"),
            SizedBox(
              height: 90,
              width: 250,
              child: RichText(
                text: TextSpan(
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w400,
                      fontSize: 32.22,
                      color: Colors.black,
                    ),
                    children: const <TextSpan>[
                      TextSpan(text: 'Enjoy your life with '),
                      TextSpan(
                        text: 'Plants',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ]),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  getstart = true;
                });
                if (getstart) {
                  // Navigate to the new page
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Loginpage()),
                  );
                }
              },
              child: Container(
                  margin: const EdgeInsets.only(left: 20, right: 20),
                  width: 320,
                  height: 50,
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Color.fromRGBO(62, 102, 24, 1),
                        Color.fromRGBO(124, 180, 70, 1)
                      ]),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.15),
                            blurRadius: 40,
                            spreadRadius: 0,
                            offset: Offset(0, 20))
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        " Get Started",
                        style: GoogleFonts.rubik(
                          fontWeight: FontWeight.w500,
                          color: const Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 30,
                        ),
                      ),
                      const Icon(
                        Icons.chevron_right,
                        size: 30,
                        color: Color.fromRGBO(255, 255, 255, 1),
                      )
                    ],
                  )),
            ),
            const Spacer(),
            Container(
                height: 42,
                width: double.infinity,
                color: const Color.fromRGBO(123, 123, 123, 1)),
          ],
        ),
      ),
    );
  }
}