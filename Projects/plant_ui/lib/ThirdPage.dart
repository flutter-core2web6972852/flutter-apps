import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pinput/pinput.dart';
//import 'package:ui_4_april2024/Loginpage.dart';
import 'package:ui_4_april2024/mainpage.dart';

class VerificationPage extends StatefulWidget {
  const VerificationPage({super.key});

  @override
  State<VerificationPage> createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  TextEditingController otpTextEditingController = TextEditingController();
  bool getstart1 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(children: [
        Column(
          children: [
            Stack(
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.arrow_back,
                            size: 26,
                          )),
                      SvgPicture.asset(
                        "images/varificationpage.svg",
                        height: 138,
                        width: 140,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 52,
                  width: double.infinity,
                  color: const Color.fromRGBO(123, 123, 123, 1),
                ),
              ],
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(left: 20, right: 20),
              height: 900,
              width: 500,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Verification",
                    style: GoogleFonts.poppins(
                        fontSize: 20, fontWeight: FontWeight.w700),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 48,
                    width: 282,
                    child: Text(
                      "Enter the OTP code from the phone we just sent you.",
                      style: GoogleFonts.poppins(
                          fontSize: 14, fontWeight: FontWeight.w400),
                    ),
                  ),
                  const SizedBox(
                    height: 34,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 10),
                    height: 56,
                    width: 340,
                    child: Pinput(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      length: 4,
                      controller: otpTextEditingController,
                      defaultPinTheme: PinTheme(
                          width: 56,
                          height: 56,
                          decoration: BoxDecoration(
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                  color:
                                      const Color.fromRGBO(204, 211, 196, 1)))),
                    ),
                  ),
                  const SizedBox(
                    height: 34,
                  ),
                  SizedBox(
                    width: 323,
                    height: 23,
                    child: RichText(
                      text: TextSpan(
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400,
                              fontSize: 15.22,
                              color: const Color.fromRGBO(0, 0, 0, 0.5)),
                          children: const <TextSpan>[
                            TextSpan(text: 'Don’t receive OTP code! '),
                            TextSpan(
                              text: 'Resend.',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ]),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        getstart1 = true;
                      });
                      if (getstart1) {
                        // Navigate to the new page
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Mainpage()),
                        );
                      }
                    },
                    child: Container(
                      margin: const EdgeInsets.only(left: 20, right: 20),
                      width: 320,
                      height: 50,
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Color.fromRGBO(62, 102, 24, 1),
                            Color.fromRGBO(124, 180, 70, 1)
                          ]),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.15),
                                blurRadius: 40,
                                spreadRadius: 0,
                                offset: Offset(0, 20))
                          ]),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            " Submit",
                            style: GoogleFonts.rubik(
                              fontWeight: FontWeight.w500,
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ])),
    );
  }
}