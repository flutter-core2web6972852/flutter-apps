import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'ThirdPage.dart';

class Loginpage extends StatefulWidget {
  const Loginpage({super.key});

  @override
  State<Loginpage> createState() => _LoginpageState();
}

class _LoginpageState extends State<Loginpage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController usernumberTextEditingController =
      TextEditingController();
  bool page3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                child: Stack(
                  children: [
                    SvgPicture.asset(
                      "images/Group 5314.svg",
                      width: 220,
                      height: 130,
                      color: const Color.fromRGBO(204, 211, 196, 1),
                    ),
                    Container(
                      height: 42,
                      width: 570,
                      color: const Color.fromRGBO(123, 123, 123, 1),
                    ),
                  ],
                ),
              ),
              Text(
                "Log in",
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w600,
                  fontSize: 32.22,
                  color: Colors.black,
                ),
              ),
              const SizedBox(
                height: 30,
                width: 30,
              ),
              SizedBox(
                height: 70,
                width: 320,
                child: TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    controller: usernumberTextEditingController,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: const Color.fromRGBO(255, 255, 255, 1),
                      label: const Text(
                        "Enter Mobile No",
                        style:
                            TextStyle(color: Color.fromRGBO(164, 164, 164, 1)),
                      ),
                      hintText: "Enter Mobile No",
                      prefixIcon: const Icon(Icons.mobile_friendly_outlined),
                      border: OutlineInputBorder(
                          borderSide: const BorderSide(
                              width: 1,
                              color: Color.fromRGBO(204, 211, 196, 1)),
                          borderRadius: BorderRadius.circular(10)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(164, 164, 164, 1)),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please Enter Mobile No";
                      }
                      if (int.tryParse(value) == null) {
                        return "Please Enter a valid Mobile No";
                      }

                      if (value.length != 10) {
                        return "Mobile No must be 10-digit number";
                      } else {
                        return null;
                      }
                    }),
              ),
              const SizedBox(
                height: 30,
                width: 30,
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    page3 = true;
                  });
                  if (_formKey.currentState!.validate()) {
                    // Navigate to the new page
                    String? number = usernumberTextEditingController.text;
                    if (number.length == 10) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const VerificationPage()),
                      );
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          backgroundColor: Color.fromARGB(255, 243, 191, 187),
                          content: Text(
                            "Please Enter Valid No",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      );
                    }
                  }
                },
                child: Container(
                    margin: const EdgeInsets.only(left: 20, right: 20),
                    width: 320,
                    height: 50,
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(colors: [
                          Color.fromRGBO(62, 102, 24, 1),
                          Color.fromRGBO(124, 180, 70, 1)
                        ]),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              blurRadius: 40,
                              spreadRadius: 0,
                              offset: Offset(0, 20))
                        ]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Log in",
                          style: GoogleFonts.rubik(
                            fontWeight: FontWeight.w500,
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 30,
                          ),
                        ),
                      ],
                    )),
              ),
              const SizedBox(
                height: 53,
                width: 60,
              ),
              Image.asset("images/Group 5315.jpg"),
              const SizedBox(
                height: 80,
                width: double.infinity,
              ),
              Container(
                  height: 52,
                  width: double.infinity,
                  color: const Color.fromRGBO(123, 123, 123, 1)),
            ],
          ),
        ),
      ),
    ));
  }
}