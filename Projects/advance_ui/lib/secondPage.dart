import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({super.key});

  @override
  State createState() => _SecondPageState();
}

class _SecondPageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Expanded(
      child: Container(
        alignment: Alignment.topLeft,
        width: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
          Color.fromRGBO(80, 3, 112, 1),
          Color.fromRGBO(197, 4, 98, 1),
        ])),
        padding: const EdgeInsets.only(top: 47),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 44),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 26,
                    width: 26,
                    child: IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: const Icon(
                          Icons.arrow_back,
                          // size: 36,
                          color: Colors.white,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 18),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 18),
                          child: Text(
                            "UX Designer from Scratch.",
                            style: GoogleFonts.jost(
                              fontWeight: FontWeight.w500,
                              fontSize: 32.61,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 11,
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 18),
                          child: Text(
                            "Basic guideline & tips & tricks for how to become a UX designer easily.",
                            style: GoogleFonts.jost(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 27,
                        ),
                        Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 15),
                              height: 34,
                              width: 34,
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(width: 1, color: Colors.white),
                                  shape: BoxShape.circle,
                                  color: const Color.fromRGBO(0, 82, 178, 1),
                                  image: const DecorationImage(
                                      image: AssetImage("assets/Group.png"))),
                            ),
                            const SizedBox(
                              width: 7,
                            ),
                            Text(
                              "Author:",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                  color:
                                      const Color.fromRGBO(190, 154, 197, 1)),
                            ),
                            Text(
                              " Jenny",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color:
                                      const Color.fromRGBO(255, 255, 255, 1)),
                            ),
                            const SizedBox(
                              width: 37,
                            ),
                            Text(
                              "4.8",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                  color:
                                      const Color.fromRGBO(255, 255, 255, 1)),
                            ),
                            Container(
                              height: 15,
                              width: 15,
                              decoration: const BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage("assets/Star 1.png"))),
                            ),
                            Text(
                              " (20 review)",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  color:
                                      const Color.fromRGBO(255, 255, 255, 0.8)),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.fromLTRB(
                  30,
                  30,
                  30,
                  20,
                ),
                width: double.infinity,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(38),
                        topRight: Radius.circular(38))),
                child: ListView.separated(
                  itemCount: 10,
                  separatorBuilder: (context, index) {
                    return Container(
                      height: 20,
                    );
                  },
                  itemBuilder: (context, index) => Container(
                    width: 300,
                    height: 70,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 8),
                            color: Color.fromRGBO(0, 0, 0, 0.15),
                            blurRadius: 40,
                          )
                        ]),
                    child: Row(
                      children: [
                        Container(
                          width: 46,
                          height: 60,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(230, 239, 239, 1),
                              borderRadius: BorderRadius.all(
                                Radius.circular(12),
                              )),
                          child: Image.asset("assets/youtube.png"),
                        ),
                        const SizedBox(
                          width: 7,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 12,
                            ),
                            Text(
                              "Introduction",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 17,
                                  color: const Color.fromRGBO(0, 0, 0, 1)),
                            ),
                            Text(
                              "Lorem Ipsum is simply dummy text ... ",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  color:
                                      const Color.fromRGBO(143, 143, 143, 1)),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
