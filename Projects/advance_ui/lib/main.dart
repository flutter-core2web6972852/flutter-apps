import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'secondPage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyUi(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyUi extends StatefulWidget {
  const MyUi({super.key});
  @override
  State createState() => _MyUiState();
}

class _MyUiState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Padding(
        padding: const EdgeInsets.only(top: 47),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20, left: 20),
              child: Column(
                children: [
                  const Row(
                    children: [
                      Icon(
                        (Icons.menu),
                      ),
                      Spacer(),
                      Icon(Icons.notifications_sharp),
                    ],
                  ),
                  const SizedBox(
                    height: 19,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Welcome to New",
                            style: GoogleFonts.jost(
                              fontWeight: FontWeight.w300,
                              fontSize: 26.98,
                            )),
                        Text("Educourse",
                            style: GoogleFonts.jost(
                              fontWeight: FontWeight.w700,
                              fontSize: 37,
                            )),
                        const SizedBox(
                          height: 15,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              label: Text(
                                "Enter your Keyword",
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    color:
                                        const Color.fromRGBO(143, 143, 143, 1)),
                              ),
                              suffixIcon: const Icon(Icons.search),
                              border: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(28.5)))),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 29,
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(38),
                    topRight: Radius.circular(38),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 20,
                    top: 33,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Course For You",
                          style: GoogleFonts.jost(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          )),
                      const SizedBox(
                        height: 16,
                      ),
                      SizedBox(
                        height: 242,
                        width: double.infinity,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const SecondPage()));
                              },
                              child: Container(
                                padding:
                                    const EdgeInsets.fromLTRB(22, 20, 18, 0),
                                width: 192,
                                height: 242,
                                decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(14)),
                                    gradient: LinearGradient(colors: [
                                      Color.fromRGBO(197, 4, 98, 1),
                                      Color.fromRGBO(80, 3, 112, 1)
                                    ]),
                                    image: DecorationImage(
                                        image: AssetImage("assets/first.png"))),
                                child: Text("UX Designer from Scratch.",
                                    style: GoogleFonts.jost(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 17,
                                        color: Colors.white)),
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: 192,
                              height: 242,
                              padding: const EdgeInsets.fromLTRB(22, 20, 18, 0),
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(14)),
                                  gradient: LinearGradient(colors: [
                                    Color.fromRGBO(0, 77, 228, 1),
                                    Color.fromRGBO(1, 47, 135, 1)
                                  ]),
                                  image: DecorationImage(
                                      image: AssetImage("assets/second.png"))),
                              child: Text("Design Thinking The Beginner",
                                  style: GoogleFonts.jost(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17,
                                      color: Colors.white)),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(22, 20, 18, 0),
                              width: 192,
                              height: 242,
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(14)),
                                  gradient: LinearGradient(colors: [
                                    Color.fromRGBO(197, 4, 98, 1),
                                    Color.fromRGBO(80, 3, 112, 1)
                                  ]),
                                  image: DecorationImage(
                                      image: AssetImage("assets/first.png"))),
                              child: Text("UX Designer from Scratch.",
                                  style: GoogleFonts.jost(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17,
                                      color: Colors.white)),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Text("Course By Category",
                          style: GoogleFonts.jost(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          )),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(25, 0, 56, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8))),
                                  child: Image.asset("assets/b1.png"),
                                ),
                                const SizedBox(
                                  height: 9,
                                ),
                                Text(
                                  "UI/UX",
                                  style: GoogleFonts.jost(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(25, 0, 56, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8))),
                                  child: Image.asset("assets/b2.png"),
                                ),
                                const SizedBox(
                                  height: 9,
                                ),
                                Text(
                                  "Visual",
                                  style: GoogleFonts.jost(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(25, 0, 56, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8))),
                                  child: Image.asset("assets/b3.png"),
                                ),
                                const SizedBox(
                                  height: 9,
                                ),
                                Text(
                                  "Illustration",
                                  style: GoogleFonts.jost(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(25, 0, 56, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8))),
                                  child: Image.asset("assets/b4.png"),
                                ),
                                const SizedBox(
                                  height: 9,
                                ),
                                Text(
                                  "Photo",
                                  style: GoogleFonts.jost(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
