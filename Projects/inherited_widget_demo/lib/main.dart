import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedData(
      data: 20,
      child: const MaterialApp(
        home: EphemeralState(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

// ignore: must_be_immutable
class SizedData extends InheritedWidget {
  int data;
  SizedData({
    super.key,
    required this.data,
    required super.child,
  });

  @override
  bool updateShouldNotify(SizedData oldWidget) {
    return data != oldWidget.data;
  }

  static SizedData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType()!;
  }
}

class EphemeralState extends StatefulWidget {
  const EphemeralState({super.key});

  @override
  State createState() => _EphemeralState();
}

class _EphemeralState extends State<EphemeralState> {
  @override
  Widget build(BuildContext context) {
    SizedData obj = context.dependOnInheritedWidgetOfExactType()!;
    obj.data = 100;
    return Scaffold(
        body: SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("${obj.data + 20}"),
          const SizedBox(
            height: 30,
          ),
          const Demo(),
          const SizedBox(
            height: 30,
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                SizedData.of(context).data = SizedData.of(context).data + 20;
              });
            },
            child: Container(
              height: 30,
              width: 30,
              color: Colors.amber,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Text("${obj.data}"),
        ],
      ),
    ));
  }
}

class Demo extends StatelessWidget {
  const Demo({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("${SizedData.of(context).data}"),
      ],
    );
  }
}
