import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:flutter/widgets.dart';

dynamic database;

class Player {
  final String name;
  final int jerNo;
  final int runs;
  final double avg;

  Player(
      {required this.name,
      required this.jerNo,
      required this.runs,
      required this.avg});

  Map<String, dynamic> playerMap() {
    return {
      'name': name,
      'jerNo': jerNo,
      'runs': runs,
      'avg': avg,
    };
  }

  @override
  String toString() {
    return '{name:$name,jerNo:$jerNo,runs:$runs,avg:$avg}';
  }
}

Future<void> insertPlayerData(Player obj) async {
  final localdb = await database;

  // Insert the player into the correct table. You might also specify the
  // `conflictAlgorithm` to use in case the same player is inserted twice.
  // In this case, replace any previous data.
  await localdb.insert(
    "Player",
    obj.playerMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<Player>> getPlayerData() async {
  final localdb = await database;
  List<Map<String, dynamic>> listPlayers = await localdb.query("Player");

  return List.generate(listPlayers.length, (i) {
    return Player(
        name: listPlayers[i]['name'],
        jerNo: listPlayers[i]['jerNo'],
        runs: listPlayers[i]['runs'],
        avg: listPlayers[i]['avg']);
  });
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  String path = await getDatabasesPath();
  print(path);
  database = openDatabase(
    join(await getDatabasesPath(), "PlayerDB.db"),
    version: 1,
    onCreate: (db, version) async {
      //This ''' demo''' is for multiLine comment
      await db.execute('''CREATE TABLE Player(
        name TEXT,
        jerNo INTEGER PRIMARY KEY,
        runs INT,
        avg REAL) ''');
    },
  );

  //INSERT INTO
  Player batsman1 =
      Player(name: 'virat kohli', jerNo: 18, runs: 20000, avg: 30.33);
  insertPlayerData(batsman1);

  Player batsman2 =
      Player(name: 'Rohit sharma', jerNo: 45, runs: 23000, avg: 10.33);
  insertPlayerData(batsman2);

  Player batsman3 =
      Player(name: 'M S Dhoni', jerNo: 07, runs: 50000, avg: 20.33);
  await insertPlayerData(batsman3);

  print(await getPlayerData());
  //runApp(const MainApp());
}
