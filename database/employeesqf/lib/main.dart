import 'package:flutter/widgets.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

dynamic database;

class Employee {
  final String name;
  final int id;
  final int experience;
  Employee({required this.name, required this.id, required this.experience});

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'id': id,
      'experience': experience,
    };
  }

  @override
  String toString() {
    return '{name:$name,id:$id,experience:$experience}';
  }
}

Future insertEmployeeData(Employee obj) async {
  final localdb = await database;

  await localdb.insert(
    "Employee",
    obj.toMap(),
    ConflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<Employee>> getEmployeeData() async {
  final localdb = await database;

  List<Map<String, dynamic>> employeeList = localdb.query("Employee");

  return List.generate(employeeList.length, (index) {
    return Employee(
        name: employeeList[index]['name'],
        id: employeeList[index]['id'],
        experience:employeeList[index]['experience']
        );
  });
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = openDatabase(
    join(await getDatabasesPath(), "employee.db"),
    version: 1,
    onCreate: (db, version)async {
     await db.execute(
          '''CREATE TABLE Employee
          (name Text,
          id INTEGER PRIMARY KEY,
           experience INTEGER) ''');
    },
  );

  //insert data into employee table
  Employee one = Employee(name: "Rohit", id: 12, experience: 2);
  insertEmployeeData(one);

  Employee two = Employee(name: "Shashi", id: 13, experience: 3);
  await insertEmployeeData(two);

  print(await getEmployeeData());
  //runApp(const MyApp());
}
