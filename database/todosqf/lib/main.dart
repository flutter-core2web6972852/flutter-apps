import 'package:flutter/widgets.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

dynamic database;

class ToDoList {
  final String title;
  final String description;
  final int date;
  ToDoList(
      {required this.title, required this.description, required this.date});

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
    };
  }

  @override
  String toString() {
    return '{title:$title,description:$description,experience:$date}';
  }
}

Future insertEmployeeData(ToDoList obj) async {
  final localdb = await database;

  await localdb.insert(
    "tasks",
    obj.toMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<ToDoList>> getEmployeeData() async {
  final localdb = await database;

  List<Map<String, dynamic>> employeeList = await localdb.query("tasks");

  return List.generate(employeeList.length, (index) {
    return ToDoList(
        title: employeeList[index]['title'],
        description: employeeList[index]['description'],
        date: employeeList[index]['date']);
  });
}

/*Future<void> updateDog(ToDoList obj) async {
  // Get a reference to the database.
  final db = await database;

  // Update the given Dog.
  await db.update(
    'Employee',
    obj.toMap(),
    // Ensure that the Dog has a matching id.
    where: 'id = ?',
    // Pass the Dog's id as a whereArg to prevent SQL injection.
    whereArgs: [obj.id],
  );
}*/

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = openDatabase(
    join(await getDatabasesPath(), "ToDoList.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute('''CREATE TABLE tasks
          (title Text,
          description Text,
           date INTEGER) ''');
    },
  );

  //insert data into employee table
  ToDoList task1 =
      ToDoList(title: "New Title", description: "new Des", date: 13);
  await insertEmployeeData(task1);

  print(await getEmployeeData());

// Update Fido's age and save it to the database.
  // one = Employee(name: one.name, id: one.id, experience: one.experience + 15);
  // await updateDog(one);

// Print the updated results.
  print(await getEmployeeData()); // Prints Fido with age 42.
  //runApp(const MyApp());
}
