import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class Ass4 extends StatelessWidget {
  const Ass4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash"),
      ),
      body: Center(
        child: Container(
            height: 150,
            width: 150,
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.red, blurRadius: 10, offset: Offset(5, 5)),
                ],
                borderRadius: BorderRadius.all(Radius.elliptical(20, 20)),
                gradient: LinearGradient(
                  colors: [
                    Colors.amber,
                    Colors.green,
                  ],
                ))),
      ),
    );
  }
}
