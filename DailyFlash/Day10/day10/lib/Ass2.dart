import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class Ass2 extends StatelessWidget {
  const Ass2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash"),
      ),
      body: Center(
        child: Container(
            height: 150,
            width: 150,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.elliptical(20, 20)),
                gradient: LinearGradient(
                    colors: [Colors.red, Colors.blue],
                    begin: Alignment.topRight,
                    end: Alignment.bottomRight))),
      ),
    );
  }
}
