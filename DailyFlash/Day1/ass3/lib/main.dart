import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("BorderAppBar"),
          backgroundColor: const Color.fromARGB(255, 225, 205, 203),
          centerTitle: true,
          shape: const BeveledRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16))),
        ),
        body: const Center(
          child: Text('Hello World!'),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
