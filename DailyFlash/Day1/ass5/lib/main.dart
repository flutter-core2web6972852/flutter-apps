import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("ShadowDemo"),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 212, 207, 207),
        ),
        body: Center(
            child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(15)),
            boxShadow: [
              BoxShadow(
                  offset: Offset(10, 10),
                  color: Color.fromARGB(255, 216, 171, 167))
            ],
          ),
        )),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
