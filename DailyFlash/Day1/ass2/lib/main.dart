import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("DailyFlash"),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 226, 211, 211),
          leading: const Icon(Icons.menu),
          actions: const [
            Icon(Icons.favorite_border_outlined),
            SizedBox(
              width: 5,
            ),
            Icon(Icons.search_off_outlined),
            SizedBox(
              width: 5,
            ),
            Icon(Icons.message),
          ],
        ),
        body: const Center(
          child: Text('Hello World!'),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
