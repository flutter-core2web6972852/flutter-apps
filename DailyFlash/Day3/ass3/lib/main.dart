import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Ass5(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Ass5 extends StatefulWidget {
  const Ass5({super.key});
  @override
  State createState() => _Ass5State();
}

class _Ass5State extends State {
  bool flag = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: ElevatedButton(
        onPressed: () {
          if (flag == false) {
            flag = true;
          } else {
            flag = false;
          }
          setState(() {});
        },
        child: Container(
          height: 300,
          width: 400,
          decoration: BoxDecoration(
              border: Border.all(
                  color: (flag!) ? Colors.red : Colors.green, width: 5)),
        ),
      )),
    );
  }
}
