// ignore_for_file: file_names

import 'package:flutter/material.dart';

class Ass2 extends StatefulWidget {
  const Ass2({super.key});
  @override
  State createState() => _Ass2State();
}

class _Ass2State extends State {
  final GlobalKey<FormFieldState> _gkey = GlobalKey<FormFieldState>();
  final TextEditingController _value = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ass2"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
        child: Column(
          children: [
            TextFormField(
              keyboardType: TextInputType.number,
              key: _gkey,
              controller: _value,
              validator: (value) {
                if (value!.contains('(') ||
                    value.contains(',') ||
                    value.contains('.')) {
                  return "Please Enter Correct Phone Number";
                } else if (value.isEmpty) {
                  return "Enter data";
                } else if (value.length < 10) {
                  return "Please Enter ten digit number";
                }
                return null;
              },
              maxLength: 10,
              decoration: const InputDecoration(border: OutlineInputBorder()),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  bool flag = _gkey.currentState!.validate();
                  if (flag) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Phone Number Entered')),
                    );
                  } else {
                    //ScaffoldMessenger.of(context).showSnackBar(
                    //const SnackBar(content: Text('wrong')),
                    // );
                  }
                },
                child: const Text("Submit"))
          ],
        ),
      ),
    );
  }
}
