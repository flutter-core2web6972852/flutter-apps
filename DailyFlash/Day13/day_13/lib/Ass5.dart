// ignore_for_file: file_names

import 'package:flutter/material.dart';

class Ass5 extends StatefulWidget {
  const Ass5({super.key});
  @override
  State createState() => _Ass5State();
}

class _Ass5State extends State {
  final GlobalKey<FormState> _mainkey = GlobalKey<FormState>();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ass5"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 10, right: 10),
        child: Form(
            key: _mainkey,
            child: Column(
              children: [
                TextFormField(
                  maxLength: 20,
                  controller: _username,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Username";
                    } else if (value.length <= 6) {
                      return "Please enter username more than 6 characters";
                    } else {
                      return "";
                    }
                  },
                  decoration: const InputDecoration(
                      labelText: "Enter Username",
                      border: OutlineInputBorder(),
                      focusedBorder: OutlineInputBorder(),
                      enabledBorder: OutlineInputBorder()),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _password,
                  decoration: const InputDecoration(
                      labelText: "Enter Password",
                      focusedBorder: OutlineInputBorder(),
                      enabledBorder: OutlineInputBorder()),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      bool flag = _mainkey.currentState!.validate();
                      if (flag) {}
                    },
                    child: const Text("Submit"))
              ],
            )),
      ),
    );
  }
}
