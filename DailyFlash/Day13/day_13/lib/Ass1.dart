// ignore_for_file: file_names

import 'package:flutter/material.dart';

class Ass1 extends StatefulWidget {
  const Ass1({super.key});
  @override
  State createState() => _Ass1state();
}

class _Ass1state extends State {
  final GlobalKey<FormState> _gkey = GlobalKey<FormState>();
  final _name = TextEditingController();
  final TextEditingController _age = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("TextFormFieldDemo"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 10, right: 10),
        child: Form(
            key: _gkey,
            child: Column(
              children: [
                TextFormField(
                  controller: _name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please enter name";
                    } else {
                      return "";
                    }
                  },
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Enter Name",
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _age,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please enter age";
                    } else {
                      return "";
                    }
                  },
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Enter age",
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      bool flag = _gkey.currentState!.validate();
                      if (flag) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Data Entered')),
                        );
                      } else {}
                      setState(() {});
                    },
                    child: const Text("Submit"))
              ],
            )),
      ),
    );
  }
}
