// ignore_for_file: file_names

import 'package:flutter/material.dart';

class Ass3 extends StatefulWidget {
  const Ass3({super.key});
  @override
  State createState() => _Ass3State();
}

class _Ass3State extends State {
  final GlobalKey<FormFieldState> _gkey = GlobalKey<FormFieldState>();
  final TextEditingController _value = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ass3"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
        child: Column(
          children: [
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              key: _gkey,
              controller: _value,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Enter data";
                } else if (!value.contains('@gmail.com')) {
                  return "Please Enter correct EmailId";
                }
                return null;
              },
              decoration: const InputDecoration(border: OutlineInputBorder()),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  bool flag = _gkey.currentState!.validate();
                  if (flag) {
                    if (_value.text.contains('@gmail.com')) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('emailId Entered')),
                      );
                    }
                  } else {
                    //ScaffoldMessenger.of(context).showSnackBar(
                    //const SnackBar(content: Text('wrong')),
                    // );
                  }
                },
                child: const Text("Submit"))
          ],
        ),
      ),
    );
  }
}
