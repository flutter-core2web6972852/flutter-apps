// ignore_for_file: file_names

import 'package:flutter/material.dart';

class Ass4 extends StatefulWidget {
  const Ass4({super.key});
  @override
  State createState() => _Ass4State();
}

class _Ass4State extends State {
  final GlobalKey<FormFieldState> _gkey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _pkey = GlobalKey<FormFieldState>();
  final TextEditingController _value1 = TextEditingController();
  final TextEditingController _value2 = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ass4"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
        child: Column(
          children: [
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              key: _gkey,
              controller: _value1,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Enter data";
                } else if (!value.contains('@gmail.com')) {
                  return "Please Enter correct EmailId";
                }
                return null;
              },
              decoration: const InputDecoration(
                  labelText: "Enter Email Id", border: OutlineInputBorder()),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              key: _pkey,
              controller: _value2,
              validator: (value) {
                if (value!.contains('(') ||
                    value.contains(',') ||
                    value.contains('.')) {
                  return "Please Enter Correct Phone Number";
                } else if (value.isEmpty) {
                  return "Enter data";
                } else if (value.length < 10) {
                  return "Please Enter ten digit number";
                }
                return null;
              },
              maxLength: 10,
              decoration: const InputDecoration(
                  labelText: "Enter Mobile Number",
                  border: OutlineInputBorder()),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  bool flag1 = _gkey.currentState!.validate();
                  bool flag2 = _pkey.currentState!.validate();
                  if (flag1 && flag2) {
                    if (_value1.text.contains('@gmail.com')) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                            content: Text(
                                'emailId Entered && phone Number Entered')),
                      );
                    }
                  } else {
                    //ScaffoldMessenger.of(context).showSnackBar(
                    //const SnackBar(content: Text('wrong')),
                    // );
                  }
                },
                child: const Text("Submit"))
          ],
        ),
      ),
    );
  }
}
