import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
            child: Container(
          height: 100,
          width: 300,
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 238, 181, 200),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              )),
          child: const Text("Your Name"),
        )),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
