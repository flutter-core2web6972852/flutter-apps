import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Ass2"),
        ),
        body: Center(
          child: Container(
            height: 100,
            width: 100,
            decoration: const BoxDecoration(
                color: Color.fromARGB(255, 233, 226, 226),
                border: Border(left:
                 BorderSide(width: 5)),
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10))),
          ),
        ),
      ),
    );
  }
}
