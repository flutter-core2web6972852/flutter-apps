import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      home: Ass5(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Ass5 extends StatefulWidget {
  const Ass5({super.key});
  @override
  State createState() => _Ass5State();
}

class _Ass5State extends State {
  bool flag = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ass5"),
      ),
      body: Center(
          child: ElevatedButton(
        onPressed: () {
          if (flag == false) {
            flag = true;
          } else {
            flag = false;
          }
          // flag = true;
          setState(() {});
        },
        child: Container(
          height: 200,
          width: 250,
          decoration: BoxDecoration(
              color: (flag)
                  ? Colors.blue
                  :  Colors.red),
          alignment: Alignment.center,
          child:
              (flag) ? const Text("Container Tapped") : const Text("Click Me!"),
        ),
      )),
    );
  }
}
