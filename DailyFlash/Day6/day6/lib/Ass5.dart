import 'package:flutter/material.dart';

class ThreeColorChangingContainers extends StatefulWidget {
  const ThreeColorChangingContainers({super.key});
  @override
  _ThreeColorChangingContainersState createState() =>
      _ThreeColorChangingContainersState();
}

class _ThreeColorChangingContainersState
    extends State<ThreeColorChangingContainers> {
  List<Color> containerColors = [Colors.white, Colors.white, Colors.white];

  void _changeColor(int index) {
    setState(() {
      containerColors[index] =
          containerColors[index] == Colors.white ? Colors.red : Colors.white;
      for (var i = 0; i < containerColors.length; i++) {
        if (i != index) {
          containerColors[i] = Colors.white;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Color Changing Containers'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center, // Center the column
          children: [
            for (int i = 0; i < containerColors.length; i++)
              GestureDetector(
                onTap: () => _changeColor(i),
                child: Container(
                  height: 100,
                  width: 200,
                  decoration: BoxDecoration(
                    color: containerColors[i],
                    border: Border.all(color: Colors.black),
                  ),
                ),
              ),
            SizedBox(height: 10), // Add spacing between containers
          ],
        ),
      ),
    );
  }
}
