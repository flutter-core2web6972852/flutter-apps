import 'package:flutter/material.dart';

class Ass2 extends StatelessWidget {
  const Ass2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Dailyflash"),
        ),
        body: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 300,
                width: double.infinity,
                decoration: const BoxDecoration(
                    image:
                        DecorationImage(image: AssetImage("assets/pizza.jpg"))),
              ),
              ElevatedButton(
                  onPressed: () {},
                  style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.purple)),
                  child: const Text("Add to cart"))
            ],
          ),
        ));
  }
}
