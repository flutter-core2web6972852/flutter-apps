import 'package:flutter/material.dart';

class Ass5 extends StatelessWidget {
  const Ass5({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("day11"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                maxLines: 5,
                decoration: const InputDecoration(
                    labelText: "Enter Your Name",
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide: BorderSide(width: 2, color: Colors.green)),
                    focusColor: Colors.amber,
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide: BorderSide(width: 2, color: Colors.red))),
              ),
            ],
          ),
        ));
  }
}
