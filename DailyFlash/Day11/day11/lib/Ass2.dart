import 'package:flutter/material.dart';

class Ass2 extends StatelessWidget {
  const Ass2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("day11"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                decoration: const InputDecoration(
                    suffix: Icon(Icons.search),
                    suffixIcon: Icon(Icons.lock),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 2, color: Colors.green)),
                    focusColor: Colors.amber,
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 2, color: Colors.red))),
              ),
            ],
          ),
        ));
  }
}
