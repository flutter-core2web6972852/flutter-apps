import 'package:flutter/material.dart';

class Ass3 extends StatelessWidget {
  const Ass3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("day11"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                textAlign: TextAlign.center,
                decoration: const InputDecoration(
                    hintText: "Enter Your Name",
                    filled: true,
                    fillColor: Colors.amber,
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(
                          width: 2,
                          color: Colors.green,
                        )),
                    enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(width: 2, color: Colors.red))),
              ),
            ],
          ),
        ));
  }
}
