import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Ass2 extends StatefulWidget {
  const Ass2({super.key});

  @override
  State createState() => _Ass2state();
}

class _Ass2state extends State {
  final TextEditingController _day = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Day12"),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              TextFormField(
                controller: _day,
                decoration: const InputDecoration(
                    labelText: "Enter Weekday",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)))),
              )
            ],
          ),
        ));
  }
}
