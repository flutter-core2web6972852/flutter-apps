import 'package:flutter/material.dart';

class Ass1 extends StatefulWidget {
  const Ass1({super.key});

  @override
  State createState() => _Ass1state();
}

class _Ass1state extends State {
  bool flag = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Day12"),
      ),
      body: Center(
        child: TextFormField(
          maxLines: 1,
          obscureText: (flag) ? false : true,
          obscuringCharacter: "*",
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              suffix: IconButton(
                  onPressed: () {
                    if (flag == false) {
                      flag = true;
                    } else {
                      flag = false;
                    }
                    setState(() {});
                  },
                  icon: (flag)
                      ? Icon(Icons.remove_red_eye)
                      : Icon(Icons.remove_red_eye_outlined))),
        ),
      ),
    );
  }
}
