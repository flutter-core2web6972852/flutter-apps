import 'package:flutter/material.dart';

class Ass1 extends StatelessWidget {
  const Ass1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Three Containers'),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment:
              MainAxisAlignment.spaceEvenly, // Space containers evenly
          children: [
            Container(
              height: 100,
              width: 100,
              color: Colors.red, // Set your desired color
            ),
            Container(
              height: 80,
              width: 80,
              color: Colors.green, // Set your desired color
            ),
            Container(
              height: 70,
              width: 80,
              color: Colors.blue, // Set your desired color
            ),
          ],
        ),
      ),
    );
  }
}
