import 'package:flutter/material.dart';

class Ass3 extends StatelessWidget {
  const Ass3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Styled Containers'),
      ),
      body:const  Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center, // Center the containers
          children: [
            ContainerWithStyle(color: Colors.red, child: Text('Container 1')),
            SizedBox(width: 10), // Add spacing between containers
            ContainerWithStyle(color: Colors.blue, child: Text('Container 2')),
          ],
        ),
      ),
    );
  }
}

class ContainerWithStyle extends StatelessWidget {
  final Color color;
  final Widget child;

  const ContainerWithStyle({required this.color, required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.only(bottomRight: Radius.circular(20), bottomLeft: Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            blurRadius: 5.0,
            spreadRadius: 1.0,
            offset: Offset(0.0, 2.0), // Shadow slightly below the container
          ),
        ],
        border: Border(
          bottom: BorderSide(color: Colors.black, width: 1.0), // Border only on bottom
        ),
      ),
      child: child,
    );
  }
}
