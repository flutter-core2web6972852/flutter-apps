import 'package:flutter/material.dart';

class Ass2 extends StatelessWidget {
  const Ass2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Star Rating'),
      ),
      body: const Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center, // Center the content
          children: [
            Icon(
              Icons.star,
              size: 40,
              color: Colors.orange,
            ),
            SizedBox(width: 10), // Add spacing between icon and text
            Text(
              'Rating: 4.5',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
