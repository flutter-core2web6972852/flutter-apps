import 'package:flutter/material.dart';

class Ass4 extends StatelessWidget {
  const Ass4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text('Proportional Containers'),
      ),
      body: Center(
        child: Row(
          children: [
            Expanded(
              flex: 6, // Container 1 takes 6 parts of the space
              child: Container(
                height: 100,
                color: Colors.red,
              ),
            ),
            Expanded(
              flex: 3, // Container 2 takes 3 parts of the space
              child: Container(
                height: 100,
                color: Colors.green,
              ),
            ),
            Expanded(
              flex: 1, // Container 3 takes 1 part of the space
              child: Container(
                height: 100,
                color: Colors.blue,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
