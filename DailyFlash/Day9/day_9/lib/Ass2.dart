// ignore: file_names

import 'package:flutter/material.dart';

class Ass2 extends StatelessWidget {
  const Ass2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("DailyFlash"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView.separated(
              separatorBuilder: (context, index) => const SizedBox(
                    height: 10,
                    width: double.infinity,
                  ),
              itemCount: 8,
              itemBuilder: (context, index) {
                return SizedBox(
                  height: 150,
                  width: 150,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.network(
                          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcq4CrV-fFNWlxD1L77zCU6Hnrpdrv28FWXT01HWLIgMGt9RxixJxD0izLhMOH03akahc&usqp=CAU"),
                      Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 3),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        alignment: Alignment.center,
                        child: const Text("Core2web"),
                      )
                    ],
                  ),
                );
              }),
        ));
  }
}
