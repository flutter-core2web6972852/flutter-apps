import 'package:flutter/material.dart';

class Ass1 extends StatelessWidget {
  const Ass1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Listview"),
          centerTitle: true,
        ),
        body: ListView(
          scrollDirection: Axis.horizontal, 
          children: [
            
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Container(
              height: 60,
              width: 60,
              color: Colors.redAccent,
            ),
            const SizedBox(width: 20,),
            Container(
              height: 60,
              width: 60,
              color: Colors.pink,
            ),    const SizedBox(width: 20,),
            Container(
              height: 60,
              width: 60,
              color: Colors.grey,
            ),    const SizedBox(width: 20,),
            Container(
              height: 60,
              width: 60,
              color: Colors.blue,
            ),    const SizedBox(width: 20,),
            Container(
              height: 60,
              width: 60,
              color: Colors.amber,
            ),  const SizedBox(width: 20,),
              Container(
              height: 60,
              width: 60,
              color: Colors.blue,
            ),    const SizedBox(width: 20,),
            Container(
              height: 60,
              width: 60,
              color: Colors.amber,
            ),
          ])
        ]));
  }
}
