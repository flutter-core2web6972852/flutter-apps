import 'package:flutter/material.dart';

class Ass5 extends StatelessWidget {
  Ass5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("3 Widgets"),
      ),
      body: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 100,
                width: 100,
                color: Colors.white,
                child: Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEIgFXOFKWbBQudY5MkdCMLVoeqGFSoKmm4pl_s9MXxR02qM99KEjtgFlmncG_P-LRwgA&usqp=CAU"),
              ),
              const Spacer(),
              Container(
                height: 100,
                width: 100,
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 100,
                width: 100,
                color: Colors.blue,
              ),
            ],
          )),
    );
  }
}
