import 'package:flutter/material.dart';

class Ass3 extends StatelessWidget {
  Ass3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Day5"),
      ),
      body:SizedBox(
        width: double.infinity,
        child: 
        Column(mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZYdmS3UoO-f063gcB2DILCzTTYP28lksVAXRL_x7b9D4Eil0YwTyeKAdVbrv0Y8DVGy8&usqp=CAU"),

            Container(
              margin:const EdgeInsets.all(20),
              padding: const EdgeInsets.only(left: 20,top: 20,right: 20,bottom: 20),
              decoration:  BoxDecoration(borderRadius:const BorderRadius.only(topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              ),
              border: Border.all(color: Colors.black,width: 2),
              ),
              child: const Text("Elon Musk"),
            )
        ],),),
    );
  }
}
