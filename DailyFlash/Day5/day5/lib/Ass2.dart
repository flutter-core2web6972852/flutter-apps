import 'package:flutter/material.dart';

class Ass2 extends StatelessWidget {
  Ass2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Column"),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEIgFXOFKWbBQudY5MkdCMLVoeqGFSoKmm4pl_s9MXxR02qM99KEjtgFlmncG_P-LRwgA&usqp=CAU"),
            ),
            Container(
              height: 100,
              width: 100,
              child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEIgFXOFKWbBQudY5MkdCMLVoeqGFSoKmm4pl_s9MXxR02qM99KEjtgFlmncG_P-LRwgA&usqp=CAU"),
            ),
            Container(
              height: 100,
              width: 100,
              child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEIgFXOFKWbBQudY5MkdCMLVoeqGFSoKmm4pl_s9MXxR02qM99KEjtgFlmncG_P-LRwgA&usqp=CAU"),
            ),
          ],
        ),
      ),
    );
  }
}
