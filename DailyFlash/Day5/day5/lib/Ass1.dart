import 'package:flutter/material.dart';

class Ass1 extends StatelessWidget {
  Ass1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile Information"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 100),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text("ProFile Photo"),
              Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZYdmS3UoO-f063gcB2DILCzTTYP28lksVAXRL_x7b9D4Eil0YwTyeKAdVbrv0Y8DVGy8&usqp=CAU"),
              const SizedBox(
                height: 20,
              ),
              const Text("Name:Elon Musk",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500
              ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text("MobileNo-8888555566",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500
              ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
