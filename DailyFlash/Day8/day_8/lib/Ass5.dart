import 'package:flutter/material.dart';

class Ass5 extends StatelessWidget {
  const Ass5({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('List with Title, Description and Icon'),
        ),
        body: ListView.builder(
          itemCount: 10,
          itemBuilder: (context, index) => Container(
            padding: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            child: Row(
              children: [
                CircleAvatar(
                  child: Icon(Icons.info),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment:
                        CrossAxisAlignment.start, // Align text left
                    children: [
                      Text(
                        'Title ${index + 1}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text('Description for item ${index + 1}'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
