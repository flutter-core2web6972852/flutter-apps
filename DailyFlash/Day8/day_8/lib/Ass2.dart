import 'package:flutter/material.dart';

class Ass2 extends StatelessWidget {
  const Ass2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Images"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                SizedBox(
                    height: 150,
                    width: 100,
                    child: Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD_M9udz6_uYkJ8CykGNW0Q_CBVmdISjMK-DlZf4pnYt3T67X_sxxtTrTCSQ&s")),
                ElevatedButton(onPressed: () {}, child: const Text("MSD"))
              ],
            ),
            Column(
              children: [
                SizedBox(
                    height: 150,
                    width: 100,
                    child: Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD_M9udz6_uYkJ8CykGNW0Q_CBVmdISjMK-DlZf4pnYt3T67X_sxxtTrTCSQ&s")),
                ElevatedButton(onPressed: () {}, child: const Text("MSD"))
              ],
            ),
            Column(
              children: [
                SizedBox(
                    height: 150,
                    width: 100,
                    child: Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD_M9udz6_uYkJ8CykGNW0Q_CBVmdISjMK-DlZf4pnYt3T67X_sxxtTrTCSQ&s")),
                ElevatedButton(onPressed: () {}, child: const Text("MSD"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
