import 'package:flutter/material.dart';

class Ass1 extends StatelessWidget {
  const Ass1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Appbar",
        ),
        actions: [
          Container(
            height: 35,
            width: 35,
            decoration: const BoxDecoration(
              color: Color.fromARGB(255, 207, 141, 218),
              shape: BoxShape.circle,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 150,
                    width: 150,
                    color: const Color.fromARGB(255, 235, 227, 150),
                  ),
                  Container(
                    height: 150,
                    width: 150,
                    color: const Color.fromARGB(255, 236, 114, 154),
                  )
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 150,
              color: Colors.green,
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      height: 150,
                      width: 150,
                      color: const Color.fromARGB(255, 215, 167, 223)),
                  Container(
                    height: 150,
                    width: 150,
                    color: const Color.fromARGB(255, 158, 190, 216),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
