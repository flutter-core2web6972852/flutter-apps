import 'package:flutter/material.dart';

class Ass3 extends StatelessWidget {
  const Ass3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Daily Flash",
          ),
          backgroundColor: Colors.blue,
        ),
        body: Center(
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
                borderRadius: BorderRadius.circular(10)),
            height: 70,
            width: 200,
            alignment: Alignment.center,
            child: Container(
              height: 80,
              width: 50,
              decoration: const BoxDecoration(
                  border: Border(
                      left: BorderSide(color: Colors.black, width: 2),
                      right: BorderSide(color: Colors.black, width: 2))),
            ),
          ),
        ));
  }
}
