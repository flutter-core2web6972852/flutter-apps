import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body:const  Center(
          child: Text('Hello World!'),
        ),
        floatingActionButton:FloatingActionButton(
  onPressed: () {},
  elevation: 0,
  child: Icon(Icons.add),
  hoverColor: Colors.orange, //<-- SEE HERE
),
      ),
    );
  }
}
