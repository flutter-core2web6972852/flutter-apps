import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: const Center(
          child: Text('Hello World!'),
        ),
        floatingActionButton: FloatingActionButton.large(
          onPressed: () {},
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [Text("Rohit"), Icon(Icons.add)],
          ),
        ),
      ),
    );
  }
}
