import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State createState() => _BottomSheetDemo();
}

class _BottomSheetDemo extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bottom Sheet"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Create Task",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w600,
                            fontSize: 22,
                          ),
                        ),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Title",
                                style: GoogleFonts.quicksand(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 15,
                                    color:
                                        const Color.fromRGBO(0, 139, 148, 1)),
                              ),
                              const TextField(
                                  decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              )),
                              const SizedBox(
                                height: 10,
                              ),
                              Text(
                                "Description",
                                style: GoogleFonts.quicksand(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 15,
                                    color:
                                        const Color.fromRGBO(0, 139, 148, 1)),
                              ),
                              const TextField(
                                  decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              )),
                              const SizedBox(
                                height: 10,
                              ),
                              Text(
                                "Date",
                                style: GoogleFonts.quicksand(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 15,
                                    color:
                                        const Color.fromRGBO(0, 139, 148, 1)),
                              ),
                              const TextField(
                                  decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              )),
                              const SizedBox(
                                height: 10,
                              ),
                            ]),
                        SizedBox(
                          width: double.infinity,
                          child: ElevatedButton(
                            onPressed: () {},
                            style: const ButtonStyle(
                                backgroundColor: MaterialStatePropertyAll(
                                    Color.fromRGBO(0, 132, 177, 1))),
                            child: Text(
                              "Submit",
                              style: GoogleFonts.inter(
                                fontWeight: FontWeight.w700,
                                fontSize: 20,
                                color: const Color.fromRGBO(255, 255, 255, 1)
                              ),
                            ),
                          ),
                        ),
                      ],
                    ));
              });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
