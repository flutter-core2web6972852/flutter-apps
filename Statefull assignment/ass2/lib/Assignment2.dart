import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});
  State<Assignment2> createState() => _Assignment2state();
}

class _Assignment2state extends State<Assignment2> {
  bool box1color = false;
  bool box2color = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Color box"),
        backgroundColor: Colors.red,
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: box1color ? Colors.blue : Colors.amber,
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        box1color = true;
                      });
                    },
                    child: const Text("color box1")),
              ],
            ),
            const SizedBox(
              width: 20,
            ),
            Column(
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: box2color ? Colors.green : Colors.deepPurpleAccent,
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        box2color = true;
                      });
                    },
                    child: const Text("color Box2")
                    ),
                    const SizedBox(
                      width: 20,
                    )
              ],
            ),
            const SizedBox(
              width: 20,
            ),
          ],

        ),
      ]),
    );
  }
}
