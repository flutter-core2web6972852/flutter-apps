import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _count = 0;
  List<String> names = [
    "Rohit",
    "Shivv",
    "Mayur",
    "Vaibhav",
    "Manoj",
    "Prajwal",
    "Rugved",
    "Darshan",
    "Athrav",
    "Sangram",
    "Vivek",
    "Abhinav",
    "Saurabh",
    "Prathamesh"
  ];
  List<String> names1 = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Snapchat",
            style: TextStyle(fontWeight: FontWeight.bold),
            selectionColor: Colors.amber),
        centerTitle: true,
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.person_3_rounded)),
          Icon(Icons.menu_open_rounded)
        ],
        leading: Icon(Icons.search),
        backgroundColor: Colors.amber,
      ),
      body: ListView.builder(
        itemCount: names1.length,
        itemBuilder: (context, index) {
          return Container(
            height: 50,
            width: double.infinity,
            child: Row(
              children: [
                Icon(Icons.person),
                SizedBox(width: 10),
                Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${names1[index]}",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text("Received" "  " "  " "00h" "  " "500")
                    ]),
                Icon(Icons.fire_extinguisher)
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _count++;
            names1.add(names[_count]);
          });
        },
        child: const Icon(Icons.skip_next),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        items: const [
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.chat,
              size: 40,
            ),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.add,
              size: 40,
            ),
            label: 'Favorites',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.favorite,
              size: 35,
              color: Colors.red,
            ),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.play_arrow,
              size: 40,
            ),
            label: 'Profile',
          ),
        ],
      ),
    );
  }
}
