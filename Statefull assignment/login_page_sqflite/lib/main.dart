import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:google_fonts/google_fonts.dart';

dynamic database;
List<LoginModel> loginDetails = List.empty(growable: true);

class LoginModel {
  final int? id;
  final String userName;
  final String password;
  LoginModel({this.id, required this.userName, required this.password});

  Map<String, dynamic> tomap() {
    return {
      'username': userName,
      'password': password,
    };
  }

  @override
  String toString() {
    return '''id:$id,
    userName:$userName,
    password:$password,
    ''';
  }
}

Future<void> addUser(LoginModel obj) async {
  final localdb = await database;

  await localdb.insert(
    "Login",
    obj.tomap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<LoginModel>> getData() async {
  final Localdb = await database;

  List<Map<String, dynamic>> details = await Localdb.query("Login");

  return List.generate(
    details.length,
    (i) => LoginModel(
        id: details[i]['id'],
        userName: details[i]['userName'],
        password: details[i]['password']),
  );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = openDatabase(
    path.join(await getDatabasesPath(), "loginDB3.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute('''
      CREATE TABLE Login(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        userName TEXT,
        password TEXT
      )
''');
    },
  );
  loginDetails = await getData();
  print(loginDetails);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: LoginPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});
  @override
  State createState() {
    return _LoginPageState();
  }
}

bool validateDetails(String username, String password) {
  for (int i = 0; i < loginDetails.length; i++) {
    if (loginDetails[i].userName == username &&
        loginDetails[i].password == password) {
      return true;
    }
  }
  return false;
}

Future<void> showData() async {
  loginDetails = await getData();
}

class _LoginPageState extends State {
  final GlobalKey<FormFieldState> _usernamekey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _passwordkey = GlobalKey<FormFieldState>();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final GlobalKey<FormFieldState> _emailKey = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _passKey = GlobalKey<FormFieldState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "To do list",
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        backgroundColor: const Color.fromRGBO(255, 166, 158, 1),
        shape: const BeveledRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10))),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
          child: Container(
              height: 800,
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
              width: double.infinity,
              decoration: const BoxDecoration(
                  color: Color.fromRGBO(224, 192, 219, 0),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(100),
                    topRight: Radius.circular(100),
                  )),
              child: Column(children: [
                const Text(
                  "",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25),
                ),
                const SizedBox(
                  height: 20,
                ),
                Image.network(
                    "https://i.pinimg.com/736x/35/45/f4/3545f49ee7278db6a6d44e62e4e9bfeb.jpg"),
                const SizedBox(height: 20),
                TextFormField(
                  key: _usernamekey,
                  controller: _username,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "enter username";
                    } else {
                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      labelText: "Enter username",
                      prefixIcon: Icon(Icons.email_outlined)),
                  keyboardType: TextInputType.emailAddress,
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  maxLength: 15,
                  key: _passwordkey,
                  controller: _password,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "enter password";
                    } else {
                      return null;
                    }
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      labelText: "Enter password",
                      prefixIcon: Icon(Icons.lock_outline_rounded)),
                  obscureText: true,
                  obscuringCharacter: "*",
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                    height: 60,
                    width: double.infinity,
                    child: ElevatedButton(
                        onPressed: () {
                          bool usernamevalidated =
                              _usernamekey.currentState!.validate();
                          bool passwordvalidated =
                              _passwordkey.currentState!.validate();

                          if (usernamevalidated && passwordvalidated) {
                            bool flag1 =
                                validateDetails(_username.text, _password.text);
                            if (flag1) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      backgroundColor: Colors.green,
                                      content: Text("Login SuccessFull")));
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text(
                                          "Please Enter Correct Details")));
                            }
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text(
                                        'Login Failed! Please enter details')));
                          }
                        },
                        child: const Text("Login"))),
                const SizedBox(
                  height: 20,
                ),
                TextButton(
                    onPressed: () {
                      showModalBottomSheet(
                          backgroundColor:
                              const Color.fromARGB(255, 232, 226, 226),
                          isScrollControlled: true,
                          context: context,
                          builder: (context) {
                            return Padding(
                              padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom,
                                left: 10,
                                right: 10,
                                top: 10,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    "Create Account",
                                    style: GoogleFonts.quicksand(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 22,
                                        color:
                                            const Color.fromRGBO(0, 0, 0, 1)),
                                  ),
                                  Image.asset("assets/demo.png"),
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Email",
                                          style: GoogleFonts.quicksand(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 13,
                                          ),
                                        ),
                                        TextFormField(
                                          key: _emailKey,
                                          controller: _emailController,
                                          validator: (value) {
                                            if (value == null ||
                                                value.isEmpty) {
                                              return "Please Enter Email";
                                            } else {
                                              return null;
                                            }
                                          },
                                          decoration: const InputDecoration(
                                              border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                          )),
                                        ),
                                        Text(
                                          "Password",
                                          style: GoogleFonts.quicksand(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 13,
                                          ),
                                        ),
                                        TextFormField(
                                          obscureText: true,
                                          obscuringCharacter: "*",
                                          key: _passKey,
                                          controller: _passwordController,
                                          validator: (value) {
                                            if (value == null ||
                                                value.isEmpty) {
                                              return "Please Enter password";
                                            } else {
                                              return null;
                                            }
                                          },
                                          decoration: const InputDecoration(
                                              border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                          )),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        SizedBox(
                                          width: double.infinity,
                                          child: ElevatedButton(
                                            onPressed: () {
                                              bool emailFlag = _emailKey
                                                  .currentState!
                                                  .validate();
                                              bool passFlag = _passKey
                                                  .currentState!
                                                  .validate();

                                              if (emailFlag && passFlag) {
                                                addUser(LoginModel(
                                                    userName:
                                                        _emailController.text,
                                                    password:
                                                        _passwordController
                                                            .text));
                                                showData();
                                                setState(() {});
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(const SnackBar(
                                                        content: Text(
                                                            "Account Created Successfully")));
                                                Navigator.of(context).pop();
                                              } else {
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(const SnackBar(
                                                        content: Text(
                                                            "Account Creation Failed")));
                                              }
                                            },
                                            style: const ButtonStyle(
                                                backgroundColor:
                                                    MaterialStatePropertyAll(
                                                        Color.fromRGBO(
                                                            89, 57, 241, 1))),
                                            child: const Text(
                                              "Submit",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 20,
                                              ),
                                            ),
                                          ),
                                        )
                                      ])
                                ],
                              ),
                            );
                          });
                    },
                    child: const Text(
                      "Don't have an account? Sign up",
                      style: TextStyle(color: Colors.blue, fontSize: 15),
                    ))
              ])),
        ),
      ),
    );
  }
}
