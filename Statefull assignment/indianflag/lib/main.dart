import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: const Text(
              "Happy 75 Republic Day",
              style: TextStyle(
                  fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            backgroundColor: Colors.orange,
            actions: [Icon(Icons.favorite_sharp)],
          ),
          body: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  height: 440,
                  width: 15,
                  color: Colors.black,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(children: [
                      Container(
                        height: 70,
                        width: 300,
                        color: Colors.orange,
                      ),
                      Container(
                        height: 70,
                        width: 300,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.network(
                                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJQAAACUCAMAAABC4vDmAAAAsVBMVEX///8BAIgAAIj///4AAIQAAID///wAAH0AAHr///oAAHYAAIv8/P3z8/a0tdEAAHKjpM3U1OTf3+3DxNqvsM7JzN/n5/C9vdru7vTJyeL4+P6rq82Li7c+PpaLjLxMTJ4wMI5WV6NpaqxiYqkoKZSWlsITE4l1dq+DhLmdnsNQUJgfIItERZhRU6Q4OZZYWp9hYZw0N5x2drcPEY8nKIqtr9mEg8NKSaMcHY9ERZDU1t58b/YzAAAUdklEQVR4nO1beX+iyNNv6AsUGA5BlIiKIuA5GTO/ZOb9v7CnqhvQJLP7xExm9p/U7ifrcjRFddW3Tgj5pE/6pE/6pE/6pE/6pE/6pPeRoIQ4bpqEYRgEAfxNXDxMbfpfchV8r6vTfj1kHIntZqcqz4L/iCVBSBKXM4sxydiQGcbQNM2haZimYRiML/JJQqn9t7kaV0cmgQfFxTPCQ0zy2beR8/fYodQJKsYZM4bAQf/P1U/4BwQnJasCl9jibzAlxidTvhCR2crIeHbQ5MZy/OcZosQdnyW7erbmjSlFB+EZrVr1vEo2Son4sxsZz/mzfWKModCWdQ5Ur4bAEhy6vsRk+z8pLSHCJe+lZA4Nzh+rMudD8ykkZAoIlSwMk1dNNTc5M/s9ZXIe0j+GEuUON6gVgMGPTZw4JN1zWcP2hMAYubP4IiEkDZoZ1yqmLIBt6j/EVLDnF4U2zPMEpBPaqPixOh3g358Rqk8If4JNr3Go8scYJP3hPHlPqD7qzZVCs8Am08gmVBAbUTKO9XUokglsZXhmaAAtVoCwsg8Xlltxpl8bWKrHGy5PeNh3+yuicfdQ4WYCeFtJuRnfcd7poFW4v1r5/ZSeWnM3DD4PBA2bLEUe/LC/ZNxLgiYN7JTtemVA7GDZ6aHJ5snHcSRIuGPt+7LFyL0KAYK8/znyO56EPya9+lDXO/JWVmwTfJA7BGMOnrQ2wR/YgjS+nJx+7RnMmv5okfY82UFC3G+txTJ2jj9G2QWZmJ2cTANMTmTJBXWWbZhCSdMLLdxe7nZRfkHrh9BTR+Ij9J0GhgZMEyIUCc8Q9v86r0FJ3nS/7qruDr/7BbzXLpzyZIduxpDF5AOElexYC8ygsVyBke/1T41mmkFBqlV70DmNunvFWPEcnKWhER5XCehve8J0p6HAlH5SrGIle/fUGje1EytpmSqW7bF0EHY75MwT1GwarIqk0RDPzE2iwez95Jy0Pg1Zc3U0671GeixbppbH9tBok3Zn78rrm0ymJT53f4spSiolJ2YyX9BeFwR9irp16x9KRyh5PLdnt0XnT2KzVz6IjcVIQx1jxW8BAwUV1XvXiMi5cEVGD0775HITaKbWTJ8Kd2V7nX1/JV1hR7YvtRHK7HdEBQClX64E6TTphSt3X7emHbEMjwpiWvpUxrwWJfLZZZ+oWzpE+Ezb4GbybgsU5JG17i6BDUi/pZcXHH0J9I9kc9BMMc2UKLSFEvHT8i9ruRWovEg7aF+8W1K05G1cwL6hIqWngHTQZx/XU/1zIRPFlGWhAolUronCVjrbXMQRrpSRVq2kTFm/F63CnVYowOGVem6wD/odHMlSy61QAqHCUszB8RYaMqvpxRH+0K6p4DrfGIIXfBdXYOQayRmzWOvugp3XYYxz4GpdkcklcAoS0kwtuYIBGrB9q1GUjHcxUXj1k1lt4Mdm75NUrCMhVsR+0MG2J/skwLPmU3ggDa2HEPNlLlGXwsUgUpwsB71GTawW4h0a+FGlYwbukXdQOleWZyxc0iOwIOMvudYrSuYyx21LNjxDbjifwMGMG4mKXqz7Foyo/yVrl1Q3unud7OzTdzA1VoIy+ej6oCD5oGyfNmIMQzz3xFGLYskhigLb2wOeK07b/WkG9auF0Qvy28NjQc463JhP06uAX+BDVo56ZTHnSjFyZsFLjyX/DmjEWY08HyQ4agr75RSD/PrhVKTTlaGCUXYrT4ROpLJdHthxdu0VhL0atMG2Jznqjc8N2J+Mo3ccS4Y2lzFLbZlwK+vwLCZwvkfOT65wRj7bg7eQdsQmvDclyf/8tDdgeP3CWmNoKZyD2sDANE6C+BJgnxYQxYF+MSUokMpMntxeUDZxsxpNNOdq7YN74wYGKs4w2RiZcbytd7lfuGt+VAmDx9hKUGdjPIQEgLYmydGAwIQWnCnpho+DTWpfJOVtM0dlYFwvHt/IVCWHynI3oQog3epp1DkyCvGK5CrYnjMOe/bIwP5qyLxgQ801bJ5kRwdDe2asQy1hQDJn9ADbLtBeH4YqbuTLm1gSlLcpN28dvYjW+1GnWyLkbIMwOuYGuMWaA+LnyFQFYQlJN4Yy2fHGAOhq5UTH892YKjkRj7fBsZzexFXEtIPBKKMlp7aOI6eVVrwx0Ls4APonEXPjAQGiITPGI1Iw49ERKC82Rikj3nv7L9VU6MhLeJZyNuC9bgthKp1QndmpgzhYOl7zQ4vDuPAwR60CzQA45CRmzCPgPdxgYzIQVAlZxsjWac94K3dRb8Ii/WooH8iM6haekiNKasgmweVVAJucklvzEVXHfIPxyiEnzipSGoMklDJwBiwntWRbR8CWgjUCgFDqLS2ZP8/Yg9hU7mJ9S8oc67rE8UV9F+KEuWRzT+lJzg1e2MnO5GnCrJhYg/SnZOHUMM8hqTigCYKuM14acj95VnHBSOOg4ioe3SCqUtkeb14ep2R6xzhHgIDwHbS8JhMG0lmC6vEBaPCclBZsYwmQvSK2oOMlaFbtkBc1M0ozqZx9/fIJ/0IzHR/G02m7RremTUU4gwRwHk2Js2Lo6+/4MY0gaFubwFA0/QEPihg3ti51JtshY4vgIiTacue6gVJ1tnkzSxBFKrc3S8LI9wNXHeqXhUgb9IUvPSoABjahOEjPnRdkNSPVLI3Y3EmemIQQfryC61gzpb1eqj2cBv73KEwPyv3xt1eHAp1XVcS23Z/512UeJX2cgQ/4uYSdY6eEeBu2cJOnPWm2mLaf7shhk7hzfs5IUmBitg0ovlDLl5vE+fJUBy5AaA2qbg4h3HkrZdo3lR0oNbPBom4ildkqmYkGVW43JuH94BuJBuF07RenbJEkA4/U1jog8T2oulFeDCWJynomH8q4PdSgpIbcf/30f6Ba+SZWp1086yTljw3bLIpMt6mwTgy6zO4ohAElyR9Phqo9Luc1vFHhihJ37hCqEGeaBFlxv+HsRxkCqOrUwy2ZKoW+OYFwKl3S4PtrQw6yg7SkfDrlSs3suIIHHxKwVK/WxXRwsbyKrDuRnsBECwyLncC/W63htsFjE1zFMGKu81Kjct6YLbsnXf8dsquqvABK/OWD6ixsHuvYJWl+lJC/e1jM73sz4DqinTyClJ243oN8mOS7bZkIeo0LWmvh33n6RqRK90NdIZPBK9mGo4MFyMo425cJSb39IK/ZdeODVY11P0pJ2hwYdgKk9Zj9YhmrrX4vkjdKCvI9VUt6qsnLOxCMU5QXKJTBlpELMRNrG0VtnsnvITSICxMNVO62fqLTjBeU79o0N3yjToVD9crL8J+KW3Y4Wj3wgWXJeTldsYucFG2n/pZLayB3K/+1jDQJOyy0YMM3bl+onOWVC6ACNULY4lnPII39utiur7odrVbttqvanzz3tahPFNeh3f+WqvPEgrfxRAKuPF/++oyTJmHsZU1Z56NIl9B9/kJQpkodAEWiLK9LPxvHQZL+ArgblZSy+PWZf2XKDYN4Mh5lzV1VfN2vTSal5FLCf7CzBzvE16u8Ml4yZRT5aq2vYxwuRcjCO9fzVVGVTTYaR3EQuuWtTOEu8BKZGme+X1fF8ngGJkCJFEOKuBwMzFl1Gr7YPtM4VD8Mi0vGuws5x1vZeXYqqrzMUHih2+jtu4WpbvueVb2dJAkmY9y9ui5HgfKH2dB8sXtMRdDuTw8vy2H/Iti/59E4Lnrj9oVsqD3Av9M09O6q0+KlojNjcfqWfw//v6RAKzr/J/t8zZTyx6vkV9ZKBZYKvGpmDCxuHe+SJXuuU8Y8yffgkKzhYzVKCP31Q0VSMA0Jb2VKg+dwcffL5dxxsbCUtsyjlCRz/kJSkKi60QFtgcnFapT+ArIpae7bNmD4+uwvKZkZ+hHW8/kQfGmATTAqiJT4LA/JNFpaz+3PZN9y6zB2SdLMJMqCy2IUkpeYnvBWvg/JGyXlnrQZmXz8bCnb9U4LZX2bdQEeZtr8wJw0YqyXFDhFD0O/femCuL49bNRwwP0yS58/O+D6DuPHWx0yaUMXuXD6ijPIKCsAmb6Y87qMEa2DHOx+npDMyiqmh0mwYou10GTJuVkhVqdxWe8RH+QyCy+2TJ09a8uEzluZulNFYVYlLT+EJtl2x/h6CfyoHiRNig1mUY5TyztS3h+wm8SM+f0daWTlkjtp8LOuCFMIghtw4dZmm/WmI9Ka6c7vG1kiZKRTDcywsPkRNidrU5ReWx/HP9kZ1MrwQM0HhYisYLrzvx4gHA6tMamsY0KiJ2Sywcs1I+G4LJ6+HJpA2S+WsJRk3x4O94kDxvp+say8MLXtS1YSFpC1sS0+mq/ddHck5ZbkKnHYQuJwlGePJifsxa36IACSQMcFKNl+9VVEjfVY0No3Jw7U0U52nobR9yZSwI2FCs2UgJfkTM5GDuac54Cs5MjdrsjqB/m2dz2+peGGYY12BNk0Y9lL64IMIotCd6ndxpursTZZaGALXFfQF8ZMk4Mh2QxSd7sCR5sBMs/cGOB/bZBcRu6e32GZSq6mxPb2YHuPL+sFEL+4LsRsaK1n8laikM5gnCCb6+S4XdHfSGuWOSCwGrCmJjEHhgrpE26RkXUA6cmI5HzIvwFoOqO95JuSkFfhoqfyXVa9PP4vNFGJnzF7Fcf+XFp8lgkEaciijK1IFownqWFNiCXTUPLENbHCuJLo0OGNxGjO+TZ4tdBBuTIe3VCgSmam3r/wKi1zhNOwwaLBt6YiA+uCBK/A2h0cThJphY7FGxTgiTjfJNZHVN0le7SYfy0qSsJQI+FTeANTFNbEPd+w1SVmFOHM2oOUlJGPJcbLdAxbEDkHg2NRwyNDVTRDrLBzbkqssMH226M5Js3d6wm3Op8xthiyyrmhQ0NHKlfEmmdf7LZza5NpFimFJ8s7YWPh5YBFvCdIjHlJHg1QqBVjj7aKlzZxm8m42XqQ98+PZOsob0ApfBmHdYXYUi9L4vuZ38/8JYBBI9jDCR+Cm9eFWN4VYhODqX6sdza4jgEo2EU2W0TqdtW/0fmVvHEip2jzAak8vHDuhk3asueI9JHxMQiDbg1WCrJHJlTJeiSNe5QR22OnZMLZ8RKTpdmmdlRSE4KbVoK6rWSNoK4HeVScQMfzkvYjDtQ9ooqA//FMdlLF/V3QFvcXxgZ4hw3M4DQN4cJOGFjZaLaeTagN+21o27ux5+dudXSU26CYdXkNgE4lzwn29J0lwx5fyM1t2wZBdmIMXdmjeoX0ySqudTltamw/lbrjur95PmGsA0oW0MB3n9VrvlmnVEDEQccWbh7xGVaZRlz9leDGAS+4sjx4n2KwumqgAKpksfNzyDHb4NmtHVtKdMDDl+4LN9EMDq6KcOmSLaYOBjo4WjLGLEakkuVYz5rzuQ6U3NWgfI7nibtqo853jEyMmC4pjK/RmBJ/kDv4FIolIASf6Yo/Cmzu8jHIqOB7F4So2qWqKUjLL+VlBdTMSJWeh4itNzOV7hUsABxeH518acMyQZZSTbwka455esAkhiGZNJVkS951rmn+xbueA3IeJW4eGObNLBEF2goUqsDrk7Po0qsZW8ruSWjtMPpLmAKlcGGp9JJurVEnH2/gdYFw6AW1pXpYgMrv6LcLMm9HJQx5bmsj4W7UreQslWQQCrf4xJTzdgRAtwQCHP5pV/IeAl3pCp+koWMWdv+uEQBwm5sudwLExjXCfdyf9HRaT2mhpiaoK6VO4/WwhEMaqxUq7F0wC1SQiJCs+2OAHO+bLKE5b0eSjUI9d9Un/gDjTxgAwuH7QaK4lBYm6jSVD+0l63XvgkmwUjlW1Zf7qveOx1J6bI2XhdQR7rfkogbjL5H+n2TzqEeVrHYAZ8VDNa1BgsH37npBcHqH2ClrM9ad805BAcVPOskE+7bt8qq24MyrFmUi3o60MKkPZNa4PVVfzUOItHQQZvSwoHlLcPeKfN2lAfQV0aXiTelo12mx3xUI+6Gup3aoS4gH/9KjJ85EeK2gQAt/Y9ZM2EU7kfwsK7HJQ19hr390UyaX8bcOlaLNszmJsZ6INtnqt8bfBHHnnVr5qqij3avXT3K6R93BodjF0uQ/9VhQt6hNsafjMVONChqz9D0QdUU0Oet5GUP6SVX8VKu5224KiSYybNkvTu0d6aDHamertSosqsQz2FB/gPDW6s+/UHBWkyngrIaMGz/xUHbx7/FC7xUlVdEyRQ/9FBIdKSsInziOYak+BjPe3k37Z7InbNgXWbHvT536EiPlnVaTss/hsv4XtfFDAwDaYf91DYs+YkwXhxQY60pikR5o7s8d+nn9y0BzcDXQnGJuEJ9716BC2Y8YaRZxN99nGpVL0knf6iTTr30yMbpMv35NOk6piBPi1EPefdASfdRnITi10srK5PvxVNcXFBIEef/aoz5+EP5ET1nC9gnHieaynfw22EfoU/cUkbQRA77sCZQ9yTxl99mltTLumaJJgzJyIyzehSsplUYNTTYL/6FW/E5yi+51ga988sCtAkXx/RIBTi4dSzdD9a4kf4pK8CkqfhoyueqStA8iyL2zDRuqz9SwNIz2CEJyr5KkeHJ5ZKQ+UVEA0E0AMdP/6M/9UD2j++7LsCHO8kuIiKaqUIdlENp+zJPE6rer8A1zzmGri+uIfAwWPOcLvMa5/zgHu9gHL0jUiOQgt6n67Ek0nGNo4AbegeuKsS4C46zOn/ouMph1Xz7pD7+MQ92U0jR2IY6KgJzucXIgq5aGdfncbsj5/a2DbjeQms7iRvelHIKpqbv/xV1TNk1Zcf0tBDe7LhKGPceR/Qd27oorgGjGX3aNsSCuv65tJ6CvznOjSS5g+6eI2tmBvWLrFzREXZrjGONf+WrUjXEsqu9kv2hsdy4JhIczAn9YRj3BY6ZZtVYf2A5fyYzpxtGuatxuYOevEU2ieoP94Rbnu2+SUb3kuYoSp+X/L5KWgDvx62q+YD09/Chqf5L+fX5eEA4rhEEMFARhkl6+0/oPSdi0i6kw6xV/7jvaT/qkT/qkT/qkT/qkT/qkD6b/A7ZyWnnO8TrvAAAAAElFTkSuQmCC"),
                          ],
                        ),
                      )
                    ]),
                    Container(
                      height: 70,
                      width: 300,
                      color: Colors.green,
                    ),
                  ],
                )
              ])),
    );
  }
}
