import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: ListViewDemo(),
    );
  }
}

class ListViewDemo extends StatefulWidget {
  const ListViewDemo({super.key});

  @override
  State createState() {
    return _ListViewDemoState();
  }
}

class _ListViewDemoState extends State {
  List<List<String>> playersList = [
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3r6OkQoIdSWxxMUUpiD1kinidC-E8Lwj3MfTcTJDutT5d0pRyt0KiFW5G908WoiYuQM4&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRV4-De3-LWee_Knze3VMDyHnP4HiV9fC74gw&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4qG0pIt64ZZWTe0YnRNFj7dczP1ZOO8lF_g&usqp=CAU",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5xdoqsMFoNG_GToD2ufXcG2BXw85SZdjNvw&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSo7NWnmZZD7SwEZYf1dwkKDGIVEIYAQPSYAQ&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMmCIQGktY0NRPyr_1GlZe0VcnumXfTj1-HQ&usqp=CAU",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRyPf294Y-KnT0c61cKRjfzJtV4DvMldhjHrA&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3GTBVMz63qWRiqMqXwG3UwexyGKCPQdQ3Uw&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQHH2x_93J7BbYxHp7IPSu8-2XQ0U6YRXqsmw&usqp=CAU"
    ],
    []
  ];
  List type = ["ODI", "TEST", "T20"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cricket Players"),
        centerTitle: true,
        backgroundColor: Colors.grey,
      ),
      body: ListView.separated(
          itemCount: 3,
          separatorBuilder: (context, index) {
            return const Divider();
          },
          itemBuilder: (context, index) {
            return Column(
              children: [
                Text(type[index]),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: playersList[index].length,
                  itemBuilder:(context,index)=>Container(
                    height: 200,
                    width: 200,
                    margin:const EdgeInsets.all(10),
                    child: Image.network(playersList[index][index]),
                  )
                   )
              ],
            );
          }),
    );
  }
}
