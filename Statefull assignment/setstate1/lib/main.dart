import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Demo(),
    );
  }
}

class Demo extends StatefulWidget {
  const Demo({super.key});
  @override
  State createState() => _DemoState();
}

class _DemoState extends State {
  int javaCount = 0;
  int flutterCount = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SetState"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 100, left: 50, right: 50),
        child: Column(
          children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    javaCount++;
                    setState(() {});
                  },
                  child: Container(
                    alignment: Alignment.center,
                    color: const Color.fromARGB(255, 232, 213, 213),
                    height: 100,
                    width: 100,
                    child: const Text(
                      "Java",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 60,
                ),
                Container(
                  alignment: Alignment.center,
                  color: const Color.fromARGB(255, 232, 213, 213),
                  height: 100,
                  width: 100,
                  child: Text(
                    "$javaCount",
                    style: const TextStyle(
                        fontSize: 30, fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    flutterCount++;
                    setState(() {});
                  },
                  child: Container(
                    alignment: Alignment.center,
                    color: const Color.fromARGB(255, 232, 213, 213),
                    height: 100,
                    width: 100,
                    child: const Text(
                      "Flutter",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 60,
                ),
                Container(
                  alignment: Alignment.center,
                  color: const Color.fromARGB(255, 232, 213, 213),
                  height: 100,
                  width: 100,
                  child: Text(
                    "$flutterCount",
                    style: const TextStyle(
                        fontSize: 30, fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
