import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            drawer: const Drawer(backgroundColor: Colors.white),
            appBar: AppBar(
              title: const Text(
                "QuizApp",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w800,
                    color: Colors.red),
              ),
              centerTitle: true,
              backgroundColor: Colors.white,
              leading: SizedBox(
                  height: 5,
                  width: 5,
                  child: Image.network(
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0KqU7xtLm15TbRb_2FbXvF1I_QTf3-Wxh70aLzWwQPAbIABV1wlWFWZW7NweoVBwjEgQ&usqp=CAU")),
            ),
            body: Center(
              child: SizedBox(
                height: 500,
                width: 320,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0KqU7xtLm15TbRb_2FbXvF1I_QTf3-Wxh70aLzWwQPAbIABV1wlWFWZW7NweoVBwjEgQ&usqp=CAU",
                        height: 80,
                        width: 80,
                      ),
                      const Text(
                        "Sign In",
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const Text("WelCome Back Good To See You!!",
                          style: TextStyle(fontWeight: FontWeight.normal)),
                      const SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 100,
                        width: 280,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text("Email"),
                            Container(
                              alignment: Alignment.center,
                              height: 30,
                              width: 280,
                              color: Colors.grey,
                              child: const Text("Enter Your Email"),
                            ),
                            const Text("Password"),
                            Container(
                              alignment: Alignment.center,
                              height: 30,
                              width: 280,
                              color: Colors.grey,
                              child: const Text("Enter Your Password"),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(Icons.check_box),
                          Text("Remember Me"),
                          SizedBox(
                            width: 80,
                          ),
                          Text("Forgot Password?")
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Colors.blue)),
                          child: const Text(
                            "Login",
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                fontWeight: FontWeight.normal),
                          )),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Or"),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Dont have an Account? Sign Up")
                    ]),
              ),
            )));
  }
}
