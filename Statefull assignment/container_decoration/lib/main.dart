import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home:  ContainerDecoration(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ContainerDecoration extends StatefulWidget {
  @override
  State createState() => _ContainerDecoration();
}

class _ContainerDecoration extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
           appBar: AppBar(
            title: const Text("Container Decoration"),
           centerTitle: true,
          // backgroundColor: Colors.blue,
           ),
           body:SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
         Container(
          margin: const EdgeInsets.only(top: 10,
         bottom: 10,
         left: 10,
         ),
         child: Container(height: 300,
         width:300,
         decoration: BoxDecoration(
          border: Border.all(
            color: Colors.yellow,
            width: 5,
          )
         ),
         ),
         ),

         const SizedBox(height: 10,),

         Container(
         height: 200,
         width: 200,
         decoration: BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(20)
         ),
         border: Border.all(
          color: Colors.blue,
          width: 5,
         )
         ),
         ),
        const SizedBox(height: 10,),

        Container(
         height: 200,
         width: 200,
         decoration: BoxDecoration(color: Colors.amber,
         borderRadius: const BorderRadius.all(Radius.circular(20)
         ),
         border: Border.all(
          color: Colors.blue,
          width: 5,
         )
         ),
        ),
          const SizedBox(height: 10,),

          Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(color: Colors.amber,
           borderRadius: const BorderRadius.all(Radius.circular(20)),
           border: Border.all(
            color: Colors.blue,
            width: 5,
           ),
           boxShadow: const [
            BoxShadow(
              color: Colors.purple,offset: Offset(10, 30),blurRadius: 8),
           BoxShadow(
              color: Colors.red,offset: Offset(20, 20),blurRadius: 8),
            BoxShadow(
              color: Colors.green,offset: Offset(10, 10),blurRadius: 8),
             BoxShadow(
              color: Colors.black,offset: Offset(8,8),blurRadius: 8),
           ]
            ),
          ),

            const SizedBox(height: 50,),

            Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(color: Colors.amber,
              borderRadius: const BorderRadius.all(Radius.circular(20)
              ),
              border: Border.all(color: Colors.blue,
              width: 5,
              ),
              gradient: const RadialGradient(
                
                stops: [0.2,0.5,0.2],
                colors: [Colors.orange, Colors.white,
                Colors.green,
                ]
              )
              ),
              
            )

           ]),)
    );
  }
}
