import 'package:flutter/material.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatefulWidget {
  MainApp({super.key});
  State<MainApp> createState() {
    return MainAppState();
  }
}

class MainAppState extends State<MainApp> {
  int? counter = 0;
  void incrementCounter() {
    setState(() {
      counter = counter! + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        drawer: Drawer(),
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            "Portfolio",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          backgroundColor: Colors.grey,
          actions: [Icon(Icons.favorite_rounded)],
        ),
        body: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              (counter! >= 1)
                  ? const Text(
                      "Name: Rohit Anil More",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  : const Text(""),
              (counter! >= 2)
                  ? Image.network(
                      "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJQAowMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABgECAwUHBAj/xAA8EAACAQMBBAYIBAMJAAAAAAAAAQIDBBEFBhIhMQcTQVFh0RYiU3GBkZKhMkJS8BQjwSQmNENjgrHS4f/EABoBAQACAwEAAAAAAAAAAAAAAAADBAECBQb/xAAoEQEAAgECBgIABwAAAAAAAAAAAQIDBBETFCExQVEFEgYiMmFxgcH/2gAMAwEAAhEDEQA/AO4gAAAAAAAFG8FXyOV9JPSBWt69TR9BrblSKxXu4PjF/pj4977AJprm2egaFc/w2pX6hcYy6UKcpyS8VFPHxIJqnTBWVzKOlaZSlbp4jO4m1Kfjhcjl85zqSc6k5TnLi5Sk22+9t8y3AHWLXpipdT/a9Hqdd/pVU4/fibfTOlbQLut1d519gnuqNSrDei2+eXHOEu9nEMIe/iB9VU6kKkIzpyU4SWYyi8prvLkz5ysNtNoLGna0KOpVo2tthKhBRW9FPlndbO3bG7VWe1NhKvbRlRrUmo1qE3lwfY89qfeBIAAAAAAAAAAAAAAAAAABpdsdSlpOzGo3tN4q06ElTfdNrCfzZ81xSUUu7tfE7l0y15UtkOri8ddcwi/dxf8AQ5Bs3pb1nW7Sw9bcqTzUceyC4yfyEzsRG7X7snTdVQm6aeHNRe6vjyLPE+jKFvRt7aFtRpQhQhHdVJL1cd2DwT2c0SpUdSWkWLk+3qI+RBxo9JuFLgKnD9cfmV34/qj8z6Lo2ltQp9XQt6NOn+mFNJGOOmaenJ/wFom+b6iPH7GeNHo4X7vnjny4k46HryVttcrfOI3VCcH4tcV/U3nSFsnZz0yrqmnW8KFxbrfqwpxSjUh28O9cyJdGkmtutJ3e2c0/d1ciSlotG6O1fq+h0CiKmzUAAAAAAAAAAAAAAABz3pqjnZe3l+m8i/tIj/RJpe7Su9WqRWZPqKT8FhyfzwvgTHpVsXe7GXbjzt5RrfCL4/Y1PRjj0OtcL/NrZ9/WSI8k/lSY43lKgAVVkAAGK5oRuberbzScasJQefFYOS9EtD+/FvGay6VKq/c0seZ19YyjnnRPpdSntlrFWsuNnv0m8cN6U3/RMnwyhyuwIFEVJ0AAAAAAAAAAAAAAAADWazSjXpdRVipUasZQnF8pJrDTI3sZZQ0/Z23taed2E6nF8368uLJZqEc0E/0vJqbelChT6umsRTbx73l/8lbJ0lYxdmQAESUAAFDX7M2UbS7v61OO7Uur+dSb70sRX2X3Njg9GnwzXzhYSbN6d9ml+27aLkVCBbVQAAAAAAAAAAAAAAAFs4xnFxkspmpuKcaVZwjyXibdnh1GlhxqJcOTIstd43SY52l4QAVlkAGQD5G2taMKUfVXFrjk1tCm6tRRxwzxNwifDHlBlnwqACdCAAAAAAAAAAAAAAAAGK5w6E8/pZlNDtXqjsLWnTpP+ZVmm1n8q5+RmKzbpDE2ivWVlKopLHJ9pkPDTnGcYzpyzF8UzPCs1+NZ9xzvttMxK/tvG7OWykorMjG6/DhH5mKUnJ5kxNiIbXSJb/Wy8Vg2SIjaasrXW6Ns5fyppxqeEny/fiS5F6lLVpG/lSvaLXnbwqADZgAAAAAAAAAAAFDx32qWdhHN1XhB9kebfwMxEz2YmYju9jGURC+2ybzGwt/99XyRH7zVr+9/xF1U3X+SL3V8kWKaW9u/RDbUVjsnep69YWCcalZTq9lOnxf/AIQLVNTqatdyuakdxfgjBPlFHk5Z9+Sylwprx4lvFp64+qrkzTds9M1J2bVOpxot/GLJFSqQrQU6UlKL5NEMylxbwu1tngltTR06bdpOVafJxh+D4spa7TYtuJvtLo/G8xmtw6Vm3+OiGr1LVY0U6VtJSqvg5dkSHS27qXf8q4t3b03zdGW98+09dtdULump29WE45448ivodPiyTvad59LfyWPU6WNrU2j2zN70stvLeW88ckz0Tay3rUo09RfU1Y+r1mMxl4+BDMFlL8yXZJnYyYa5I2lwqZLUnd1ylWp1oKdKcZxfJxeS9M5RQuK9tNTt61SlJdsJNZN3Y7W31DhcwhcQXNv1ZfMp30lo7dVmupie6eg0un7S6fetQdTqKr/JV4fJ8jcRkpLK5FWazXpMLEWiey4AGGQAACjKllacaVOVSfCMYuTfggIxtXrtW0qqysqm5UxmpUXOK7l4kNlOU5uU5OUnzcnlsy3lzK7uqtxN+tUk5eRhydfDjilXNyXm1gqUyMkqNTODHCWIRjBZkks55IySw4td5SCUIqK5ICip8U6mZPx5L4EY2j0+NtVVzRWKdSWJJcoy8mSo1O01WFPTHTazKpNKOezHHP77ylrsVb4J+3h2vgNTlw66kU89JRa0oTurmnQp/im8e7vZN7a0o29vClShiMVzXNvvbIts7VjS1WCml/Mi4Jvsf7WCY8ir8Tjr9Jv5dX8WanNOauCf07b/AMyx704fizKPf2r3laeN6eOTeV8kX5LYRjBycfzczrvILgMjIBm00fXLrTasFKrOdtn1qT48O9dxq8lGa3pW8bS2raazvDrdKpGrThUhJSjJJprtReR7Yy8/iNK6mTzKhLd+HNfvwJAce9fraYdKtvtESqADVsHi1i3rXenV7a3lGNSrHdzJvGO37HtKNGYnbqxMb9EF9DtQ9tbfVLyHodqHtrb6peROgT81l9oeXogvobqHtrb6peQ9DdQ9tbfVLyJ0BzOX2cCiC+h2oe2tvql5D0O1D21t9UvInQHM5fZy9EF9DtQ9tbfVLyNJrnR5rV/XpOlc2SpwjhKUpc/kdVKEWXJbLX627LWkyTpMsZcfdxyHRdr1OcZwu7DejJNPfn/1JStjtQws17XOOOJS8idNcSuDTDPB3+nlNrtXk123G67f0gvodqHtrb6peQ9DdQ9tbfVLyJ0Cfmcvtz+XogvobqHtrb6peQ9DdQ9tbfVLyJ0DPM5fZwKIL6Hah7a2+qXkPQ7UPbW31S8idAczl9nL0R3ZrRbzSbmrKtUoyp1IpNQbzlPg+XiyRlCpDa03neUtaxWNoAAatgAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//9k=")
                  : Container(),
              (counter! >= 3)
                  ? const Text(
                      "Collage:  Sinhgad Institute of Technology Science Narhe",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  : const Text(""),
              (counter! >= 4)
                  ? Image.network(
                      "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAMAAzAMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABAYBAwUHAgj/xAA7EAABBAEDAgMHAQYEBwEAAAABAAIDBBEFEiEGMRNBURQiMmFxgZEHFSMzQmKhFiRS4VRygqKxwdJT/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAECAwQF/8QAKREBAAICAQIFAwUBAAAAAAAAAAECAxEhBBIFEzEyUUFxoRUiQlKBFP/aAAwDAQACEQMRAD8A9xREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBFjKZQZREQEREBERAREQEREBERAREQEREBEWCUGVjPOFptW4KcDp7UrIom8ue84AXDv9RO8W3UqQO9phZG8NcPee1+MOa3zHxj6sKCwOka0EuIAHcnyUO5rFCkSLNljCIny/9DAC4/YEflcK1pOpXtUZYityuqBsL2e0YGHtc9r/AHQMYfG/nju1pWWdMaLGyKEvlMMb3EVo3kxgOiMTmAc7WEHO0EAEZGEHan1itXlpxWmyQOtymGISDHv4yAfr2Hz4UCfqJrdAj1WOuTue1hgJy5uZPD8vQrfJFQMQjmrGwxob7tqTfnaQ5p98nJBAIPqsxSVYWFtetUiaTuIGBk5zngepU6lG4cq51XLWsWoG1YZ3wRbv3MhPPhF+Tx8OWkZ+ins10vvw04mxF0sLpmyGT3HNbsBAPmcv+2PmpHt5HDRXxjHDnf8AyntofgPbWOO2XHj8tTU/BuGl3UkAdYbHBI8wWo6rsEDLn7NpHyJfj7Fb4uoNOkc5jpjG9pcHB4+ENIaSfQAnGStW3T5JA91Gu6QOY7eNmcsztPrxk49MqLPo2kTxSRuZYhinw2ZoJImG8v2uPJwSTnBHBI7KNG4d+OeKUExPa8AlpLTnBHBC2A58lXhpLoa2pHTrh8W23+JHj9y8j3ngepPvYyocOvahpxY3WqwaA5sbmRh0hb7gcXb/AOYb3bGjGTjuTlErcijV7kc2Gcxy+ccnDuwPb7hSMoMoiICIiAiIgIiICIiDBWi9cgo13T25WxRN+J7uwyt5zkeir1+W1bvyUGMk8KQtBl8MPiMf8wyCCx4PbOecd+UGnSJL+p3rceoGKzQcHN27GmIj+QxuA5BaSHNcSQQecYUqIUdKrwiIeLJBEIG2ZzudtHYE4y7z7f2X3qNmGlWdFAxrImHljBjc4844/J+vzVZmmksSeJMcuI4A7AegW2LDN+Wd8na6ljV5JshoL/6pO32YDj85Wr2mxL8crseg4H4ChRBTIgujy61Y98y2xsz3yVJijy1RJbLIBhuHPzyM9kh1QMA3Q5+jljbLWJ07KdJlvXuiHQES+XR8gcqP+2o/+Hd93LU7Vw5w/wAvx/z/AOyp59V46HN/VtkjHotBL4zlj3NPyOFkX2yytaIg0H1cvuZq2pet3Nlw5MXuhrbqE0bgXtZJjzOWuH0cOVOr6pFcZ7PY99rjyyXAd3yMOGAcfY/NcmUcKK4eS0nDW8cMfMtV2b2lWLF9t6lceQ9zWSl/8SCNvPhsxgjc7lxJzwByMY6Oj6uy857AHmNrzHDZdwyztxkt7eeR6HBwTgrkaTqMkcrYnuye0bj6/wCk/I/2U3Vo4WvqatPb8KnVGGwucyJkbnHaX5IzuAJAHHc+q470mk6l0UvFoWNFE06/W1Gs2xUkL4i4tDtpGcHHmpaosIiICIiAiIgIiIMHuqnoNOvD1JdfC/xZWtJlIqiPY5xzguHcnv27YP1tjlVNX3abq9aSBzGixYBjhjjG6V5B3EuOT2b2aEGNfD/3LnDvJLn67sD/ALcLk4KtepVWTQmR4LY5MOcSOY3Y7n5Y4P0Vas1pq0vhTNw7y9HfMK0571jUOnpumwZZ3fmWsOI7FfXjPHZxUW5ar0qktq1KI4Yhl8hB4C1XdSpUabLdyw2OCQtDZCCQSew4WE5Lz6y9GMGCnERCYSSST3PmsDhRNWvx6Zpdq/K0uZXjL3NaeT6AKu9H9bw9S3Zansjq8jGGQe9kOGeVXUzy0nJSsxX5W1Fr3SmH2gMb7M1waXbufPy+yr3W+q29LgpvoOk8R0wMjI4w4mMEZPP1A+6iI2WyRWsz8LKtsc0jBhpyPQrzLp3rqX/EHsOoSyTVLjw6CeWMRmMH4Rjzb816Rg+Q/CtzSeFK3x568pDpg4fNR3d18RTRTNLoZGSNBLSWOBAI7jjzWzGV0U6u9fo5L+GYre2Zh8t3EtDPizx9fJXFxlZQtGERl4c4x+IAW7uO/I/mz5ri6TQeXsme0FxGYmHz/qPy/wDKndQTzU6Da8EViTxP4kkDA57G+bgCCCckcHyz97Xyzk1uHBbBGK0xE7SOn69irBLHZ8L+MS3wq/hDkZP8xzznn7LrrmaDUjqafGyJ0jmvO/dKTudnnkHt9Bwums0CIiAiIgIiICIiAtNieODaZHbQ44z5LaVzNQZbLHB2x0ROQWsO4f3WHUZJx45tEb0mI3LoBwIDgcj5eag2KLHtMbAwsOT4L+31HmFCilfEc15HMBPI7j7g/wCyiHVK9q/JXlsRi3HgeF8JAxkEA+ufL0XkR47imm+2e6Po2jFaJ9VZ/UXoi5r2msg0y6ab2OLnxTkiOXjGC4cflVTrnT+p4eiIq02mkuYWCxPEfF37TwWYBwMgHPHb5r0+TqNtK0+tMZSGPZHudHubudyBkc/lSquqaXbAfEWAyc7q8u3f+CCV24fEcGSImf2/dM3yRuZn14eP631LJp36e6e+YCxdv1xFmYbgRj3i7Pfj1XB/SjUNIpXLAuFsN3Ydk0j8MLPMfIr37UNO0fVWiLUY6tst+GO7VZJsz6DAIP3XBtfpl0XZd4kmk0S4/wD5TSwj8Nfj+y7K3x29toktnt3xb4VrQ9Rs6tLdkhmYzQGTf5d5bh0uGjcc/wCkHPKaizxK0d2Qe/YuQbWu42xNd7jf7l2PIuKvv+GtOFNtKOvBHXa0MEcdhwAaPJfc+gadYYyOxFXc1jmvH+Zfw5vLTxj0Tj5b/wDRTt0/PfXtRkHUcVKvH70DxKZg7LhG8sDGn0Dew+qsXUljXq/WlCPSPabWK7QIWMc9kZcNpc5re45J5/K9cPTegC1LYlp6ZJNM4Pe99YSOJHY5cT28uFNmuUKrmRTWJS54Jja53hhwHftgEcjv6qts+GnutDDv3vt+rzHpPpnqhvUE17UpGUaE5MstY43PfgjIjbkgHOT5+q9OqaS2D94Rt295JucfRvb8qFP1NRqubDXMTS44DIm7jnBPyHYd+yV9Tr3tMr6jLMPZ54mysdM4YDXDI+XbC8/qPFsWGImtZn/E1nJMa3wskELImnAJceXPdyXfUr5nkr8Qz7XeIcCNwzn7Li0tVfepxy1pwYDkMka3+IAcZz/7WyASGZr67d785JIznjzOVT9YpfLGLHWZmfwz8qYjcu9kLK1QCbYPHLS/+kYAW1ezE8MhERSCIiAiIgIiwThBlfJGSuDY6nb7ZbradpOoamaRDbL6vhBsb8A7Pfe3LsEHAypH7ejfLpLYqV2SPUo/EZK2IbIRt3fvDn3eOPPlRI6E1WGbJewbv9Q4P5XL/wAN0jrcGrP3PnhidG3cB5+vHlzj6ldrPK4lnqFkHVVbQHUrAlsQPnZYJb4bmt7gc5zkgcgLKcGPv7+3n5TufRLn0itK4O2BrgQQe+CO3ft3PbHdcO90XVnZZEbGxmaNseYD4eA0uIAA9dxzzyPRWsOHAWcgcrG/Q4bzvWvtwt3yptPpybT9QFphl2e/ljdxJB27RwcEDB7+qiy6drsJeYdSJB3FgEQBZlw2j392cNz6ZKvmPVcXUtfFDXtL0l1Kdx1Bz2ssBzBG3a0uIIzuzx6Y57rkt4TSbbi0/haMsuRqkeonb+zZgOMe+xvxZ+Igt5GM9iPooNnTddnkLfbi6LxCWFga1wAYcdmA8uwSM9uOcqy9W9R1+ldJfql2rasVo3ASez7CW57HDnD+y6laZ09aOZ8MkJe0O8N+C5vyO0kfgquPwetP5fhE5NqP/he7YsSvmlnIlka53vOyQI2jv2GHAngc5HIU2v0dmWrPMXGxEWmR7nDEn7tzCMc9w8k89wDnhXDAChanq9HSW1jenEftVhleAbSS+R5w1owt6+G4/WZlHfKBQ6YoUtvhRMZta1obE3aAAMD1/IwVu1bp+jqukz6dLFtimaBub8TcdiD6rd+0pTrrtN/Z1oQiAS+27R4JOcbM5znzWzRtWpa3p8d/TZfGqyEhkm0tDsHBIzzjI7rqp02LH7YVm0vmvpVaCKOINLmRtDWtzhoA7DHoprWNYNrQAPQJLKyGJ8kp2sY0ucfQDuo2k6nU1jToNR0+R0lWdu6N5YW7h64PKvjw48fsrEEzMpqL53t25zwgcD25WqH0iIgIiICIiAsO7LKwRlB5preizVn671B0r1NJp0zHvkt1p9roHvjbznPLcgDlfTdS1C1qnQNu9JLVs3Q82qzZC1jsQvIyz8FXqXRNKltG1LptN9gkOMroGlxI7HOO63T6bRsTNnsUq8szcbZJImucMHI5Iz3QeS6x476HX841S/v0qy19QttuAjOwHyPPJPBXb1OOPVOvumYrFh4E2jzl7oZixzs7exHIV8bpGmsEgZp9RvijEm2Bo3j0PHKM0nTY5GSR6fUbIzGx7YGgtx2wccIPK62sWq36fUZp9RJhg1x9eds1rZJYgD3gRB7j8XY8kcNOSurWv19H0XqDXbt+Z1F8zWVqla8yV1aN5Y3Bc1xa0lxPIccDt6K0a/0+20Kn7PrUxHFZE1ipJEGstAAgbiB3BIcCc9lq0zpOnFrNrVbFClB7RWFb2SBgMRYHbiXDADjkDy4QU6lrV3TKnWztFsNsuqVo7FeKO0bLYXOadxa49wMZx8lmC1o1zqroi3pmoNtTyiQzn2kyHcYXZ3DOGuz9PNeoRUakL/EirQMfs2bmxgHb/p+nyWtmk6dGxjI6FVrGO3Ma2BoDT6jjugqH61uaP071EFwBJZjJ7+8Fr19rJ+t+laxuWI4LVSyJY4rDmNk2tZtzg/Mq82Kla0GizBFNt+HxGB2PplaxplAFhFKsCwAMPgt93HbHHCDy/STqFvpiXTqesNbPD1C+GBl2V5bYY1pf4Dn/ABAEAnP9OPNQ9dte2aHoU+owO09lXqiKKcstl0LQHHc9r/Qc4Plz2Xrcul6fMx7JqNWRryC9roWkOxnGeOe5/JX3JQqSVm1pKsDq7fhidG0sH27IKRUdCf1NfTgvzOgk0cFrBZLhncBuHPfHmFTNKmrRfoxUs0tQkbZrzxmwIrDswjxeQQDwNq9rFKq2QStrQiQdniMZHGO/0RtGo2OWNlaFscpzI0RjDz8x5oPO2ahU1TrLX2adqTrNOXQQ5hhsFzDJucHFnkeAAcehHcFV7TtTj0/oTpI1LzW032WR6q58pcyH3XYa8A5Y3cPl2XszKleMsMcETNg2s2sA2j0HosNo1WwvhbWhbE8kvYIxtcT6jsUHnNjTp26THDpXUmnTSTawJq9cyk1yBGXGuXBxIGAX/gY5Vo6AtyXNDMktNtV4sSsLI5TJGSHEEscf5T5LuP06k+uKz6kDq45ERiaWA/TGFIYxrGtawBrWjAAGAEH0iIgIiICIiAiIgIiICIiDGEDQPX8rKICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiD/9k=")
                  : Container(),
              (counter! >= 5)
                  ? const Text(
                      "Dream Compony: Nvidia",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  : const Text(""),
              (counter! >= 6)
                  ? Image.network(
                      "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAKgAswMBIgACEQEDEQH/xAAcAAEAAwADAQEAAAAAAAAAAAAABQYHAwQIAgH/xABIEAABAwMBBAMKCgYLAQAAAAAAAQIDBAURBgcSITETUYEUFiJBYXGRk9HhCBUyQlV0grLB8CMzN1JydTU2Q1SSlKGxwsPxF//EABoBAQADAQEBAAAAAAAAAAAAAAABAwUCBAb/xAAsEQEAAgECAwYFBQAAAAAAAAAAAQIRAwQSMYEhQVFhodE0UnGRsQUTFCIy/9oADAMBAAIRAxEAPwDcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcc80dPE6WZ7WRsTLnOXCIVz46rrxM+CyMbFAxf0lXMnLzJ+ewo1demliJ7ZnlHeibRCdulX3BQT1W5v9E3e3c4yVbv5X6PT13uPxUpO6UbT3Oorbki+BI9VWFjvL4sek5K6S5UjN6+W2lraT50kLfCb+fypm6+51r/2pbhiPLijrPd6q7WmeT47+V+j09d7h38u+j09d7jo3CwQz0a3GxyrNBzdCvFzOtE9nMrnsM3W3u+0Zxa305Yn0Vze8NL07e1vTZ1WnSHolany97Oc+TyEyU/Z5+rrvOz8Sl7ctX3/AE1drZDZLk+ljmp3Oka1jHbyo7GeKKfQbDVtq7et7zmZ911Jma5lsgPPNFW7Zq6ghr6SSrlpp40lie1tP4TVTKLjnyO9oLa3e26ip7Lq1rZWSz9zumdEkcsMirhN5ETCpvcF4Jjn4sHrdt4B581przV2lNoNTQz3WaS2w1TJmwrDGm/A5Uduou7leCq3OfEpt9/u8Np09XXdVR8dNTOnb1Pw3LU7VwnaBKAxLYtqjVmq9STrdbtLPbqOBXyM6KNqOe7g1Fw1F/eX7JH6F15qe57Taa0V11kloH1M7HQrFGiKjWvVEyjc+JPGBvoPPWvNd6xotoFws1lusjI0qGRU8CRRLxcjcJlzetfGp8XXUG1/TUKV14dUMpWu8JzoIJGJ/ErEXCegD0QCg7LtoCa0tlSyqiZBdKNqLK2P5EjVzh7c8uKcU8XDrKHsg13qfUGtIaC73V9TSrBI5Y1ijblUThxRqKBvQILW9+ZprStxuzlTfgiXoUVM70i+CxP8Sp2ZPOztpG0FlvjuC3mbuWSZ0LZegixvtRrlT5PU5oHqYEXpm8Q6gsFDdqbHR1UKPVqLnddyc3scip2EoAAAEdqC3LdLXLTNXEnyo1z85OXsKtZVfX2Kssmehq41V7Gr4O/xyrV7eHanUXogb3p9KyZtbQy9zVzOKPTk5fL7TP3e2ta37tIzOMTHjHu4tXvhDUlbTd6tbQOY2CshjVJI1TCv48/L5Tp2q43eqt/xVQsV+Vx0y/MYvNM8kJKfu172OuVrjirWLiO4IqbjV63eTznzP080asr9S0kUS82U2OPowZtq3zGJmMRjw7PPPLplVOXwtXT6dpFt9sd3TcplxI9iZRrupE6/J6SOqdLXSOlSqcxJHuy6SNrvDb7ewkKa62GyNX4tglqp8Y6V6Yz2ry7EIq7ajr7m10bnpDAv9nHwynlXxlOvbb8GNS2ccoryjr3otNcdqc2efq67zs/Ey/4Sn9O2f6q/7xqGzz9XXedn4lQ22aKv+qrtbZ7JRJPHBTuZI5ZWMwquzjiqGz+mfCU6/mV2n/mF+2ef1F0//LoPuIefNrrUp9rNwWLwV6WnemOGFWONVX08T0XoyiqLbpOz0NbH0dTT0UUUrMou65GoiplOHMyDaVs71PfNoFZdrZb2y0ciwq2Tp2Nzusai8FXPNFPe7cnwkrJh9qv0beaLRzOz53s/7P8AQj9U6vSr2H2Sh32rVVMiUkrc5XcgXOfPwiX7Rr20jT79TaMuNtgZv1LmJJTplEzI1d5EyvBM43e0wNNkmupI46d1uakLXq5qOqo91quwiu59TW58wGr7ArJ8WaJ7ulZia5TLLlW4Xo2+C1PNwcqfxGU7NP2yUf1yp+5Iel7VQQ2u2UlvpUVIKWFkMaL+61ERP9jEdD7PdT2raTS3mutyR0Lamd7pOnYuGua9EXCLnmqekCs6y/bg/wDmtN/wPRWp1pG6cui3Hd7k7kl6be5bu6uTFNdbPNY3DXtwvVmoEWN1QyWnm6eNFy1rcLhV608ZwXXSG1nUMCUd4mklplVFVklXG1iqnLKNXiB0vg9b/frW7vL4sl3vNvx/jg6uwP8AaJT/AFaX7prmyzZ4ui6CrlrJ46i51jUSRY0XcjanJrVXivHiq8PF1caXsj2fam05rGC43e3pBSpBI1zkmY7CqnDgiqA+EdqDelt2noH8GJ3VUInWuWsT0b648qH5faXSybG4LLT321SXOiY2sRrKtiudPxWRqYXjwc5qeZDjds31FqjaJLddT29YLVU1DnyqlQxXJG1uGM8FVXkjW5Tyl4/+MaK/uNR/mn+0Cs/Bz1Es1DX6dnfl1OvdNOirx3HcHonkR2F+2ptBgmmtnurNJ7QY7lbrf0trhq3sRyVDEV9M5VTiirlV3VzhfGhsFlrLxUXK4RXKiZDSxvVKd7UVN5M4TjnjlOPD8cIE2AAAAA4K2ljraWSmm3ujkTDt1cKQnedauuo9Z7ixAp1Nvpas5vWJRNYnmrvebauuo9Z7h3m2rrqPWe4sQK/4O2+SPsjgr4I+0WeltLZUpN/9IqK7fdnl/wCkgAeilK0rw1jEJiMcgAHSQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/9k=")
                  : Container(),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              incrementCounter();
            });
          },
          child: const Text("Next->"),
        ),
      ),
    );
  }
}
