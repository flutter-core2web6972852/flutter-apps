import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State createState() => _PortfolioState();
}

class _PortfolioState extends State {
  bool flag = false;
  final TextEditingController _namesTextEditingController =
      TextEditingController();
  final TextEditingController compony = TextEditingController();
  final TextEditingController location = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _namesTextEditingController.addListener(() {
      print(_namesTextEditingController.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("PortFoliio"),
        centerTitle: true,
        backgroundColor: Colors.purple,
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          const Text(
            "Enter Your Name-",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            height: 50,
            width: 300,
            child: TextField(
              controller: _namesTextEditingController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)))),
            ),
          ),
          const Text(
            "Enter Your Dream Compony-",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 50,
            width: 300,
            child: TextField(
              controller: compony,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)))),
            ),
          ),
          const Text(
            "Enter Compony Location",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 50,
            width: 300,
            child: TextField(
              controller: location,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)))),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
                setState(() {
                  flag = true;
                });
              },
              child: const Text(
                "Submit",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )),
          (flag == true)
              ? Container(
                  margin: const EdgeInsets.all(30),
                  // color: Colors.grey,
                  decoration: BoxDecoration(border: Border.all()),
                  child: Column(children: [
                    const Text(
                      "Employee Detail",
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text("Employee Name=${_namesTextEditingController.text}"),
                    const SizedBox(
                      height: 5,
                    ),
                    Text("Compony Name=${compony.text}"),
                    const SizedBox(
                      height: 5,
                    ),
                    Text("Compony Location=${location.text}"),
                  ]),
                )
              : Container()
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Text("ADD"),
      ),
    );
  }
}
