import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget {
  Assignment1({super.key});

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      title: const Text("Demo_app",
      style: TextStyle(
        fontStyle: FontStyle.italic,
        fontWeight: FontWeight.bold,
      ),
      ),
      backgroundColor: Colors.red,
      actions: [
        Icon(Icons.account_box_rounded),
        SizedBox(
          width: 10,
        ),
         Icon(Icons.favorite_rounded),
      ],
    ));
  }
}
