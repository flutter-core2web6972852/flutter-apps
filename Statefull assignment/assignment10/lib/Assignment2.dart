import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget {
  Assignment2({super.key});

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("AppBar"),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
        actions: [
           Icon(Icons.alarm_off_rounded),
        SizedBox(
          width: 10,
        ),
         Icon(Icons.menu_rounded),
        ],
      ),

    );

  }
}