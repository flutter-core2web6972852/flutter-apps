import 'package:flutter/material.dart';

class Assignment4 extends StatelessWidget {
  Assignment4({super.key});

  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
        title: const Text("Hello Core2web"),
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
      ),
      body: 
      
      Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
         children: [
          Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
          SizedBox(
            width: 20,
          ),
      
          Container(
            width: 100,
            height: 100,
            color: Colors.amber,
          ),
          
         ],
        ),
      ),
    );
  }
}
