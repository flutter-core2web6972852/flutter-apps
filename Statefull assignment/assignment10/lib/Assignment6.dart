import 'package:flutter/material.dart';
class Assignment6 extends StatelessWidget {
  Assignment6({super.key});

  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
        title: const Text("Hello Core2web"),
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
      ),
      body:
      Center(child:
      SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
            width: 200,
            height: 200,
            color: Colors.blue,
          ),
          SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.black,
          ),
           SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.deepOrange,
          ),
            SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.greenAccent
          ),
            SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.redAccent
          ),
            SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.deepPurple
          ),
            SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.pink
          ),
            SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.black12
          ),
            SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.brown
          ),
            SizedBox(
            height: 20,
          ),
      
          Container(
            width: 200,
            height: 200,
            color: Colors.cyan,
          ),
        ]),
      ), 
      )
      
    );
  }
}