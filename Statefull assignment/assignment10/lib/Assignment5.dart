import 'package:flutter/material.dart';

class Assignment5 extends StatelessWidget {
  Assignment5({super.key});

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Hello Core2web"),
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Image.asset("assets/image1.png"),
                SizedBox(
                  width: 10,
                ),
                Image.asset(""),
                SizedBox(
                  width: 10,
                ),
                Image.asset("")
              ],
            )
          ],
        ),
      ),
    );
  }
}
