import 'package:flutter/material.dart';
//import 'ListViewDemo.dart';
//import 'ListViewDemo1.dart';
import 'ListViewBuilderdemo.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: //ListViewdemo(),
    //  ListViewDemo1(),
    ListViewBuilderdemo(),
      debugShowCheckedModeBanner: false,
    );
  }
}
