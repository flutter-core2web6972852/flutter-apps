import 'package:flutter/material.dart';

class ListViewBuilderdemo extends StatefulWidget {
  const ListViewBuilderdemo({super.key});

  State<ListViewBuilderdemo> createState() => _ListViewBuilderdemostate();
}

class _ListViewBuilderdemostate extends State<ListViewBuilderdemo> {
  int _count = 0;

  List<int> numbers = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ListViewBuilderDemo"),
        centerTitle: true,
      ),
      body: ListView.builder(
          itemCount: numbers.length,
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.all(10),
              color: Colors.grey,
              alignment: Alignment.center,
              child: Text("${numbers[index]}"),
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _count++;
            numbers.add(_count);
          });
        },
        child: const Text("=>"),
      ),
    );
  }
}
