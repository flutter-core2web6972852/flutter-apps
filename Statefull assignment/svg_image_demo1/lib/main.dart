import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: SvgImagedemo(),
    );
  }
}

class SvgImagedemo extends StatefulWidget {
  const SvgImagedemo({super.key});

  @override
  State<SvgImagedemo> createState() => _SvgImagedemoState();
}

class _SvgImagedemoState extends State<SvgImagedemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SvgImageDemo"),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 144,
              width: 144,
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromRGBO(234, 238, 235, 1)),
              child: SvgPicture.asset('assets/Group77.svg'),
            )
          ],
        ),
      ),
    );
  }
}
