import 'package:product_display/models/ProductModel.dart';
import 'package:flutter/material.dart';

class ProductController extends ChangeNotifier {
  ProductModel? productModel;

  void addData({required ProductModel pObj}) {
    productModel = pObj;
  }
  // ProductController({required this.productModel});

  void addToFavorite() {
    if (productModel!.isFavorite) {
      productModel!.isFavorite = false;
    } else {
      productModel!.isFavorite = true;
    }
    notifyListeners();
  }

  void addQuantity() {
    productModel!.quantity = productModel!.quantity + 1;
    notifyListeners();
  }

  void removeQuantity() {
    productModel!.quantity = productModel!.quantity - 1;
    notifyListeners();
  }
}
