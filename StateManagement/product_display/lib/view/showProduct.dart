import 'dart:developer';

import 'package:flutter/material.dart';

import 'package:product_display/controllers/ProductController.dart';
import 'package:product_display/models/ProductModel.dart';
import 'package:provider/provider.dart';

class ShowProduct extends StatefulWidget {
  const ShowProduct({super.key});

  @override
  State<ShowProduct> createState() => _ShowProductState();
}

class _ShowProductState extends State<ShowProduct> {
  @override
  Widget build(BuildContext context) {
    ProductModel? obj = Provider.of<ProductController>(context).productModel;
    return Scaffold(
        body: Center(
      child: Container(
        child: Column(
          children: [
            Image.network(obj!.imageUrl),
            const SizedBox(
              height: 15,
            ),
            Text(obj.name),
            const SizedBox(
              height: 15,
            ),
            Text(obj.price),
            const SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Consumer<ProductController>(builder: (context, provider, child) {
                  log("IN FAVROITE CONSUMER");
                  return GestureDetector(
                    onTap: () {
                      Provider.of(context, listen: false).addToFavorite();
                    },
                    child: Icon(
                      (obj.isFavorite)
                          ? Icons.favorite_rounded
                          : Icons.favorite_outline_rounded,
                    ),
                  );
                }),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Provider.of<ProductController>(context, listen: false)
                            .addQuantity();
                      },
                      child: const Icon(Icons.add),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Consumer<ProductController>(
                        builder: (context, value, child) {
                      return Text("${obj.quantity}");
                    }),
                    const SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Provider.of<ProductController>(context)
                            .removeQuantity();
                      },
                      child: const Icon(Icons.remove),
                    )
                  ],
                )
              ],
            )
          ],
        ),
      ),
    ));
  }
}
