import 'package:flutter/material.dart';
import 'package:product_display/controllers/ProductController.dart';
import 'package:product_display/view/get_productdetail.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) => ProductController(),
      child: MaterialApp(
        home: GetProductDetails(),
      ),
    );
  }
}
