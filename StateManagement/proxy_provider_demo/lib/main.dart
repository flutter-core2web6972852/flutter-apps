import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class Login {
  String userName;
  String password;

  Login({required this.userName, required this.password});
}

class Employee {
  int empID;
  String empName;
  String userName;
  String password;

  Employee(
      {required this.empID,
      required this.empName,
      required this.userName,
      required this.password});
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("in myapp build");
    return MultiProvider(
      providers: [
        Provider(create: (context) {
          return Login(userName: "rohit1902", password: "Shivro@1926");
        }),
        ProxyProvider<Login, Employee>(
          create: (context) {
          log("in employeee create");
          return Employee(
            empID: 12,
            empName: "Rohit More",
            userName: Provider.of<Login>(context, listen: false).userName,
            password: Provider.of<Login>(context, listen: false).password,
          );
        }, 
        update: (context, login, employee) {
          log("in employee update");
          return Employee(
              empID: 12,
              empName: "Rohit More",
              userName: Provider.of<Login>(context).userName,
              password: login.password);
        })
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ProxyProviderDemo(),
      ),
    );
  }
}

class ProxyProviderDemo extends StatefulWidget {
  const ProxyProviderDemo({super.key});

  @override
  State<ProxyProviderDemo> createState() => _ProxyProviderDemoState();
}

class _ProxyProviderDemoState extends State<ProxyProviderDemo> {
  @override
  Widget build(BuildContext context) {
    log("in ui build");
    return Scaffold(
      appBar: AppBar(
        title: const Text("Proxy Provider Demo"),
        centerTitle: true,
        backgroundColor: Colors.grey,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(Provider.of<Employee>(context).empName),
            const SizedBox(
              height: 10,
            ),
            Text("${Provider.of<Employee>(context).empID}"),
            const SizedBox(
              height: 10,
            ),
            Text(Provider.of<Employee>(context).userName),
            const SizedBox(
              height: 10,
            ),
            Text(Provider.of<Employee>(context).password),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
