import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class Login with ChangeNotifier {
  String userName;
  String passWord;

  Login({required this.userName, required this.passWord});

  void changePassword(String passWord) {
    this.passWord = passWord;
    notifyListeners();
  }
}

class Employee with ChangeNotifier {
  int empID;
  String empName;
  String userName;
  String passWord;

  Employee(
      {required this.empID,
      required this.empName,
      required this.userName,
      required this.passWord});
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    log("In MyAPP BUILD");
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) {
          return Login(userName: "Rohit1926", passWord: "Shivro@1926");
        }),
        ChangeNotifierProxyProvider<Login, Employee>(create: (context) {
          log("In Employee create");
          return Employee(
              empID: 19,
              empName: "Rohit",
              userName: Provider.of<Login>(context, listen: false).userName,
              passWord: Provider.of<Login>(context, listen: false).passWord);
        }, update: (context, login, employee) {
          log("in employee update");
          return Employee(
              empID: 19,
              empName: "Rohit",
              userName: login.userName,
              passWord: login.passWord);
        })
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ChangeNotifierProxyProviderdemo(),
      ),
    );
  }
}

class ChangeNotifierProxyProviderdemo extends StatefulWidget {
  const ChangeNotifierProxyProviderdemo({super.key});

  @override
  State<ChangeNotifierProxyProviderdemo> createState() =>
      _ChangeNotifierProxyProviderdemoState();
}

class _ChangeNotifierProxyProviderdemoState
    extends State<ChangeNotifierProxyProviderdemo> {
  @override
  Widget build(BuildContext context) {
    log("in ui build");
    return Scaffold(
      appBar: AppBar(
        title: const Text("ChangeNotifierProxyProviderDemo"),
        centerTitle: true,
        backgroundColor: Colors.amber,
      ),
      body: Center(
        child: Column(
          children: [
            Text("${Provider.of<Employee>(context).empID}"),
            const SizedBox(
              height: 15,
            ),
            Text(Provider.of<Employee>(context).empName),
            const SizedBox(
              height: 15,
            ),
            Text(Provider.of<Employee>(context).userName),
            const SizedBox(
              height: 15,
            ),
            Consumer(builder: (context, login, child) {
              log("in consumer");
              return Text(Provider.of<Employee>(context).passWord);
            }),
            const SizedBox(
              height: 15,
            ),
            ElevatedButton(
                onPressed: () {
                  Provider.of<Login>(context, listen: false)
                      .changePassword("Password Changed");
                },
                child: const Text("Change Password"))
          ],
        ),
      ),
    );
  }
}
