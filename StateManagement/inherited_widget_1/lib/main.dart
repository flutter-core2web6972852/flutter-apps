import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const SizedData(
        data: 25,
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: ShowData(),
        ));
  }
}

class ShowData extends StatelessWidget {
  const ShowData({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("${SizedData.of(context).data}"),
      ),
    );
  }
}

class SizedData extends InheritedWidget {
  final int data;

  const SizedData({super.key, required this.data, required super.child});

  @override
  bool updateShouldNotify(SizedData oldWidget) {
    return data != oldWidget.data;
  }

  static SizedData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType()!;
  }
}
