import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return Employee(
          compName: "google",
          empCount: 250,
        );
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ShowData(),
      ),
    );
  }
}

class ShowData extends StatefulWidget {
  const ShowData({super.key});

  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {
  final fName = "Rohit";
  @override
  Widget build(BuildContext context) {
    log("in showData build");
    return Scaffold(
      appBar: AppBar(
        title: const Text("ChangeNotifierProvider_demo"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(Provider.of<Employee>(context).compName),
            const SizedBox(
              height: 25,
            ),
            Text("${Provider.of<Employee>(
              context,
            ).empCount}"),
            const SizedBox(
              height: 25,
            ),
            ElevatedButton(
                onPressed: () {
                  Provider.of<Employee>(
                    context,listen: false,
                  ).changeName("microsoft", 500);
                },
                child: const Text("Change Company Name")),
            NormalClass(
              name: fName,
            ),
          ],
        ),
      ),
    );
  }
}

class NormalClass extends StatelessWidget {
  final String name;
  const NormalClass({super.key, required this.name});

  @override
  Widget build(BuildContext context) {
    log("In Normal class Build");
    return Text(name);
  }
}

class Employee extends ChangeNotifier {
  String compName;
  int empCount;

  Employee({
    required this.compName,
    required this.empCount,
  });

  void changeName(String compName, int empCount) {
    this.compName = compName;
    this.empCount = empCount;
    notifyListeners();
  }
}
