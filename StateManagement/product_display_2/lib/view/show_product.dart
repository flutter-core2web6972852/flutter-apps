import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:product_display_2/controllers/product_controller.dart';
import 'package:product_display_2/controllers/wishlist_controller.dart';
import 'package:product_display_2/models/product_model.dart';
import 'package:product_display_2/view/show_wishlist.dart';
import 'package:provider/provider.dart';

class ShowProduct extends StatefulWidget {
  const ShowProduct({super.key});

  @override
  State<ShowProduct> createState() => _ShowProductState();
}

class _ShowProductState extends State<ShowProduct>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("All products"),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const WishListScreen()));
                },
                icon: const Icon(Icons.favorite_rounded))
          ],
        ),
        body: ListView.builder(
            itemCount:
                Provider.of<ProductController>(context).allProducts.length,
            itemBuilder: (context, index) {
              return SizedBox(
                height: 260,
                width: 200,
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(
                          height: 150,
                          width: 150,
                          child: Image.network(
                              Provider.of<ProductController>(context)
                                  .allProducts[index]
                                  .imageUrl)),
                      const SizedBox(
                        height: 15,
                      ),
                      Text(Provider.of<ProductController>(context)
                          .allProducts[index]
                          .name),
                      const SizedBox(
                        height: 15,
                      ),
                      Text(Provider.of<ProductController>(context)
                          .allProducts[index]
                          .price),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Consumer<ProductController>(
                              builder: (context, provider, child) {
                            log("IN FAVROITE CONSUMER");
                            return GestureDetector(
                              onTap: () {
                                Provider.of<ProductController>(context,
                                        listen: false)
                                    .addToFavorite(index);
                                if (Provider.of<ProductController>(context,
                                        listen: false)
                                    .allProducts[index]
                                    .isFavorite) {
                                  ProductModel obj = ProductModel(
                                      Provider.of<ProductController>(context,
                                              listen: false)
                                          .allProducts[index]
                                          .imageUrl,
                                      Provider.of<ProductController>(context,
                                              listen: false)
                                          .allProducts[index]
                                          .name,
                                      Provider.of<ProductController>(context,
                                              listen: false)
                                          .allProducts[index]
                                          .price);
                                  Provider.of<wishlistController>(context,
                                          listen: false)
                                      .addWishListData(obj);
                                }
                              },
                              child: Icon((Provider.of<ProductController>(
                                          context,
                                          listen: false)
                                      .allProducts[index]
                                      .isFavorite)
                                  ? Icons.favorite_rounded
                                  : Icons.favorite_outline_rounded),
                            );
                          }),
                          Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Provider.of<ProductController>(context,
                                          listen: false)
                                      .addQuantity(index);
                                },
                                child: const Icon(Icons.add),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Consumer<ProductController>(
                                  builder: (context, value, child) {
                                return Text(
                                    "${Provider.of<ProductController>(context).allProducts[index].quantity}");
                              }),
                              const SizedBox(
                                width: 10,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Provider.of<ProductController>(context,
                                          listen: false)
                                      .removeQuantity(index);
                                },
                                child: const Icon(Icons.remove),
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              );
            }));
  }
}
