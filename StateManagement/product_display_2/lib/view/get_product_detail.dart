import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:product_display_2/controllers/product_controller.dart';
import 'package:product_display_2/models/product_model.dart';
import 'package:product_display_2/view/show_product.dart';
import 'package:provider/provider.dart';

class GetProductDetails extends StatelessWidget {
  GetProductDetails({super.key});
  final TextEditingController _url = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _price = TextEditingController();
  final GlobalKey<FormFieldState> _urlKey = GlobalKey();
  final GlobalKey<FormFieldState> _nameKey = GlobalKey();
  final GlobalKey<FormFieldState> _priceKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("GetProuductDetail"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 50, left: 15, right: 15),
        child: Column(
          children: [
            Text(
              "Enter Product Details",
              style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w700, fontSize: 30),
            ),
            const SizedBox(
              height: 15,
            ),
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter Prouct URL';
                }
                return null;
              },
              key: _urlKey,
              controller: _url,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                hintText: "Enter Product URL",
                hintStyle: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please Enter Product Name';
                }
                return null;
              },
              key: _nameKey,
              controller: _name,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  //  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(8),
                ),
                hintText: "Enter Product Name",
                hintStyle: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please Enter Product Price';
                }
                return null;
              },
              key: _priceKey,
              controller: _price,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  // borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(8),
                ),
                hintText: "Enter Product Price",
                hintStyle: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            ElevatedButton(
                onPressed: () {
                  bool urlFlag = _urlKey.currentState!.validate();
                  bool nameFlag = _nameKey.currentState!.validate();
                  bool price = _priceKey.currentState!.validate();
                  if (urlFlag && nameFlag && price) {
                    ProductModel obj =
                        ProductModel(_url.text, _name.text, _price.text);
                    Provider.of<ProductController>(context, listen: false)
                        .addData(pObj: obj);
                  }
                  _url.clear();
                  _name.clear();
                  _price.clear();
                },
                child: const Text("Add Product")),
            const SizedBox(
              height: 15,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return const ShowProduct();
                  }));
                },
                child: const Text(" Show Product"))
          ],
        ),
      ),
    );
  }
}
