import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:product_display_2/controllers/product_controller.dart';
import 'package:product_display_2/controllers/wishlist_controller.dart';
import 'package:provider/provider.dart';

class WishListScreen extends StatefulWidget {
  const WishListScreen({super.key});

  @override
  State<WishListScreen> createState() => _WishListScreenState();
}

class _WishListScreenState extends State<WishListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Favorite"),
        centerTitle: true,
      ),
      body: ListView.builder(
          itemCount: Provider.of<wishlistController>(context).wishList.length,
          itemBuilder: (context, index) {
            return SizedBox(
              height: 180,
              width: 150,
              child: Column(
                children: [
                  SizedBox(
                      height: 50,
                      width: 50,
                      child: Image.network(Provider.of<wishlistController>(
                              context,
                              listen: false)
                          .wishList[index]
                          .imageUrl)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(Provider.of<wishlistController>(context)
                          .wishList[index]
                          .name),
                      const SizedBox(
                        height: 15,
                      ),
                      Text(Provider.of<wishlistController>(context)
                          .wishList[index]
                          .price),
                      Consumer<wishlistController>(
                          builder: (context, provider, child) {
                        log("IN FAVROITE CONSUMER");
                        return GestureDetector(
                          onTap: () {
                            Provider.of<wishlistController>(context,
                                    listen: false)
                                .removeFromWishlist(index: index);
                            Provider.of<ProductController>(context,listen: false)
                                .addToFavorite(index);
                          },
                          child: const Icon(Icons.favorite_rounded),
                        );
                      }),
                    ],
                  )
                ],
              ),
            );
          }),
    );
  }
}
