import 'package:flutter/material.dart';
import 'package:product_display_2/models/product_model.dart';

class ProductController extends ChangeNotifier {
  List<ProductModel> allProducts = [];

  void addData({required ProductModel pObj}) {
    allProducts.add(pObj);
  }
  // ProductController({required this.productModel});

  void addToFavorite(int index) {
    if (allProducts[index].isFavorite) {
      allProducts[index].isFavorite = false;
    } else {
      allProducts[index].isFavorite = true;
    }
    notifyListeners();
  }

  void addQuantity(int index) {
    allProducts[index].quantity = allProducts[index].quantity + 1;
    notifyListeners();
  }

  void removeQuantity(int index) {
    allProducts[index].quantity = allProducts[index].quantity - 1;
    notifyListeners();
  }
}
