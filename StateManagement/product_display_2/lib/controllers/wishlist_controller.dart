import 'package:flutter/material.dart';
import 'package:product_display_2/models/product_model.dart';

// ignore: camel_case_types
class wishlistController extends ChangeNotifier {
  List wishList = [];

  void addWishListData(ProductModel obj) {
    wishList.add(obj);
  }

  void removeFromWishlist({required int index}) {
    wishList.removeAt(index);
    notifyListeners();
  }
}
