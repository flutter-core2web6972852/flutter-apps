import 'package:flutter/material.dart';
import 'package:product_display_2/controllers/product_controller.dart';
import 'package:product_display_2/controllers/wishlist_controller.dart';
import 'package:product_display_2/view/get_product_detail.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (BuildContext context) => ProductController(),
        ),
        ChangeNotifierProvider(create: ((BuildContext context) {
          return wishlistController();
        }))
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: GetProductDetails(),
      ),
    );
  }
}
