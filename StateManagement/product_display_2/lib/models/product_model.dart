class ProductModel {
  String imageUrl;
  String name;
  String price;
  bool isFavorite;
  int quantity;

  ProductModel(
    this.imageUrl,
    this.name,
    this.price, {
    this.isFavorite = false,
    this.quantity = 0, 
  });
}
