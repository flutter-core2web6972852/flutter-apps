import 'package:flutter/material.dart';
import 'package:inherited_widget/playerinfo.dart';

class ShowData extends StatefulWidget {
  const ShowData({super.key});

  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Player Info"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("PlayerName is-${PlayerInfo.of(context).playerName}"),
              const SizedBox(
                height: 10,
              ),
              Text("CountryName- ${PlayerInfo.of(context).countryName}"),
              const SizedBox(
                height: 10,
              ),
              Text("Team is-${PlayerInfo.of(context).team}"),
              const SizedBox(
                height: 10,
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    PlayerInfo.of(context).team = "CSK";
                  });
                },
                child: Container(
                  height: 30,
                  width: 100,
                  color: Colors.grey,
                  child: const Text("Change Team"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
