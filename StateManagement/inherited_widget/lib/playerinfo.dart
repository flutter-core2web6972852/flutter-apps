import 'package:flutter/material.dart';

class PlayerInfo extends InheritedWidget {
   String playerName;
   String countryName;
   String team;

   PlayerInfo({
    super.key,
     required this.playerName,
    required this.countryName,
    required this.team,
    required super.child,
  });

  @override
  bool updateShouldNotify(PlayerInfo oldWidget) {
    if (playerName != oldWidget.playerName) {
      return true;
    } else if (team != oldWidget.team) {
      return true;
    }
    return false;
  }

  static PlayerInfo of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType()!;
  }
}
