import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inherited_widget/playerinfo.dart';
import 'package:inherited_widget/showdata.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _playerName = TextEditingController();
  final TextEditingController _country = TextEditingController();
  final TextEditingController _team = TextEditingController();
  final GlobalKey<FormFieldState> _playerKey = GlobalKey();
  final GlobalKey<FormFieldState> _countryKey = GlobalKey();
  final GlobalKey<FormFieldState> _teamKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("InheritedDemo"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              TextFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter player name';
                  }
                  return null;
                },
                key: _playerKey,
                controller: _playerName,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    // borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hintText: "Enter Player name",
                  hintStyle: GoogleFonts.poppins(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter Count ry';
                  }
                  return null;
                },
                key: _countryKey,
                controller: _country,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    //  borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hintText: "Enter Country",
                  hintStyle: GoogleFonts.poppins(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter Player team';
                  }
                  return null;
                },
                key: _teamKey,
                controller: _team,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    // borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  hintText: "Enter Enter team Name",
                  hintStyle: GoogleFonts.poppins(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                  onPressed: () {
                    bool nameFlag = _playerKey.currentState!.validate();
                    bool countryFlag = _countryKey.currentState!.validate();
                    bool teamFlag = _teamKey.currentState!.validate();
                    if (nameFlag && countryFlag && teamFlag) {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return PlayerInfo(
                          playerName: _playerName.text,
                          countryName: _country.text,
                          team: _team.text,
                          child: const ShowData(),
                        );
                      }));
                    }
                  },
                  child: const Text("Login")),
            ],
          ),
        ));
  }
}
