import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) => const Employee(
            empName: "ROHIT",
            empID: 19,
          ),
        ),
        ChangeNotifierProvider(
            create: (context) =>
                Company(empName: "Shashi", empID: 65)),
      ],
      child: const MaterialApp(
        home: ShowData(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

class ShowData extends StatefulWidget {
  const ShowData({super.key});

  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
              Text(Provider.of(context).empName),
            const SizedBox(
              height: 20,
            ),
              Text("${Provider.of(context).empID}"),
            const SizedBox(
              height: 20,
            ),
               Text(Provider.of(context).empName),
            const SizedBox(
              height: 20,
            ),
              Text("${Provider.of(context).empID}"),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
                onTap: () {
                  Provider.of<Company>(context, listen: false)
                      .changeData("Shivv", 25);
                },
                child: const Text("change compony data"))
          ],
        ),
      ),
    );
  }
}

class Employee {
  final String empName;
  final int empID;

  const Employee({required this.empName, required this.empID});
}

class Company extends ChangeNotifier {
  String empName;
  int empID;
  Company({required this.empName, required this.empID});

  void changeData(String empName, int empID) {
    this.empName = empName;
    this.empID = empID;
    notifyListeners();
  }
}
