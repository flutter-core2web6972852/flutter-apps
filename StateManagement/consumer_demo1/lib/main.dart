import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    log("In myapp build");
    return MultiProvider(
      providers: [
        Provider(create: (context) {
          return Player(jerNo: 07, playerName: "M.S.Dhonii");
        }),
        ChangeNotifierProvider(create: (context) {
          return Rohit(userName: "Rohit", id: 15);
        }),
        ChangeNotifierProvider(create: (context) {
          return Match(matchNo: 23, runs: 5000);
        }),
        ChangeNotifierProvider(create: (context) {
          return Employee(role: "Java Developer", salary: 2500);
        })
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MatchSummery(),
      ),
    );
  }
}

class MatchSummery extends StatefulWidget {
  const MatchSummery({super.key});

  @override
  State<MatchSummery> createState() => _MatchSummeryState();
}

class _MatchSummeryState extends State<MatchSummery> {
  @override
  Widget build(BuildContext context) {
    log("in match Summery build");
    return Scaffold(
      appBar: AppBar(
        title: const Text("Consumer Demo"),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Text(Provider.of<Player>(context).playerName),
            // const SizedBox(
            //   height: 20,
            // ),
            // Text("${Provider.of<Player>(context).jerNo}"),
            // const SizedBox(
            //   height: 20,
            // ),
            Consumer3<Match, Rohit, Employee>(
                builder: (context, match, provider, employee, value) {
              log("in consumers");
              return Column(
                children: [
                  Text("${Provider.of<Match>(context).matchNo}"),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(Provider.of<Rohit>(context).userName),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(Provider.of<Employee>(context).role),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              );
            }),
            ElevatedButton(
                onPressed: () {
                  Provider.of<Match>(context, listen: false).changeData(26);
                  Provider.of<Rohit>(context, listen: false)
                      .changeInfo("MayurrS");
                  Provider.of<Employee>(context, listen: false)
                      .changeRole("Flutter Developer");
                },
                child: const Text("Update Match Data")),
            const SizedBox(
              height: 20,
            ),
            // const NormalClass(),
          ],
        ),
      ),
    );
  }
}

class NormalClass extends StatelessWidget {
  const NormalClass({super.key});

  @override
  Widget build(BuildContext context) {
    log("In Normal Class Build");
    return Text("${Provider.of<Match>(context).matchNo}");
  }
}

class Player {
  int jerNo;
  String playerName;

  Player({required this.jerNo, required this.playerName});
}

class Match with ChangeNotifier {
  int matchNo;
  int runs;

  Match({required this.matchNo, required this.runs});

  void changeData(
    int matchNo,
  ) {
    this.matchNo = matchNo;
    notifyListeners();
  }
}

class Rohit extends ChangeNotifier {
  String userName;
  int id;

  Rohit({required this.userName, required this.id});

  void changeInfo(String userName) {
    this.userName = userName;
    notifyListeners();
  }
}

class Employee extends ChangeNotifier {
  String role;
  int salary;

  Employee({required this.role, required this.salary});

  void changeRole(String role) {
    this.role = role;
    notifyListeners();
  }
}
