import 'package:flutter/material.dart';

class SkillModel extends InheritedWidget {
   List skill;
   SkillModel({
    super.key,
    required this.skill,
    required super.child,
  });

  @override
  bool updateShouldNotify(SkillModel oldWidget) {
    return skill != oldWidget.skill;
  }

  static SkillModel of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType()!;
  }
}
