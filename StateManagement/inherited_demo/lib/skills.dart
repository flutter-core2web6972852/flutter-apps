import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'skillModelPage.dart';

class SkillsPage extends StatefulWidget {
  const SkillsPage({super.key});

  @override
  State<SkillsPage> createState() => _SkillsPageState();
}

class _SkillsPageState extends State<SkillsPage> {
  final TextEditingController _skill = TextEditingController();
  final GlobalKey<FormFieldState> _idSkil = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 100.0, left: 10, right: 10),
        child: Column(
          children: [
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please  Enter skill ';
                }
                return null;
              },
              key: _idSkil,
              controller: _skill,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  // borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(8),
                ),
                hintText: "Enter skill",
                hintStyle: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  bool flag = _idSkil.currentState!.validate();
                  if(flag){
                    SkillModel(skill: ,
                    )
                  }
                },
                child: const Text("Add Skill")),
            Expanded(
                child: ListView.builder(
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return Container();
                    }))
          ],
        ),
      ),
    );
  }
}
